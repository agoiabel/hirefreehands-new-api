<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>To freelancer</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu'" rel="stylesheet" type="text/css">
    <style>
        html, body {
            font-family: 'Ubuntu', sans-serif;
            background: #efefef !important;
            width: 100%; 
        }
        
        
        h1 {
            font-size: 2em;
            color: #0BC773;
        }
        
        h2 {
            font-size: 1.2em;
        }
        
        .btn {
            padding: 12px 24px;
            border-radius: 6px;
            background-color: #0AC775;
        }
        
        hr {
            border: 0;
            position: relative;
            
            background: #0BC773;
        }
        
        .footer {
            text-align: center;
        }
        
        .footer a {
            display: inline;
            text-decoration: none;
        }
        
        a {
            text-decoration: none;
        }
        
        p {
            color: #111;
            font-weight: 100;
            font-size: 1em;
        }

    </style>
</head>
<!-- body -->

<body style="background: #efefef !important; padding-top: 50px !important;">
	<center>
		<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background: #fff; padding: 70px;">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#">
                                    <img style="height: 25px;padding-top: 0px;" src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <tr>
                <td>
                    <table border="0" style="padding-top: 5px;">
                        <tr>
                            <td style="width: 70%;">
                                <h1 style="font-size: 2em; color: #0BC773;"> Congratulations {{ $freelancer->name }}, <br/>You’ve Been Hired!</h1>

                            </td>
                            <td style="display: inline; width: 30%;">
                                <img style="width: 100%; margin-top: 22px;" src="https://s30.postimg.org/7uvr2lsld/Bid-_Approved.png">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <!--horizontal line-->
                <td>
                    <hr style="width: 100%; height: 1px; margin-top:30px;border: 0;position: relative;background: #0BC773;">
                </td>
            </tr>
            <!--end here-->
            <tr>
                <!-- call to action-->
                <td style="border-spacing: 3em;">
                    <p style="padding: 20px 0 30px 0; color: #111;font-size: 1em;">
                        Your bid for the job, <b> {{ $job->title }},</b> was successful and the client has hired you to work on this project. Please visit the job on the platform to sign your job contract and get started. Once you sign, you'll gain access to a Workstream where you can communicate with your client and deliver the job.


                    </p>

                    <a href="https://hirefreehands.tech/jobs" class="btn" style="text-decoration: none; padding: 12px 24px;border-radius: 6px; background-color: #0AC775;"><b style="color: #fff">Go to Job</b></a>

                    <p style="padding-top: 40px">
                        Congratulations again. Be sure to knock this one out of the park.<br><br> Have a lovely day, <br> The Hirefreehands Team.

                    </p>
                </td>
            </tr>
			<!--end here-->
		</table>

 		<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding-top: 40px;">
            <tr>
                <td class="footer" style="text-align: center;">

                    <tr>
                        <td style="text-align:center">
                            <a href="https://www.facebook.com/hirefreehands/">
                                <img style="margin: 0px;" height="16px" src="https://s30.postimg.org/66nv9rf35/facebook.png">
                            </a>
                            <a href="https://twitter.com/hirefreehands">
                                <img style="margin: 0px; margin-left:20px;" height="20px" src="https://s30.postimg.org/ppsgj4dup/twitter.png">
                            </a>
                            <a href="https://www.linkedin.com/company-beta/10832378/">
                                <img style="margin: 0px; margin-left:20px;" height="16px" src="https://s30.postimg.org/rvmrdmhb5/linkedin.png"><br/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center">
                            <img src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png" height="30" style="margin: 10px; padding-top: 26px;">

                            <p style="margin-bottom: 40px;  font-size: 10px; color: #999;">Need any help? Get in touch with us on <a href="mailto:help@hirefreehands.tech" target="_top">help@hirefreehands.tech</a>
                                <br>&copy; Hirefreehands 2017. All Rights Reserved</p>
                        </td>
                    </tr>
                </td>
            </tr>
        </table>
    </center>
</body>

</html>