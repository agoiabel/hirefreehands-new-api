<!DOCTYPE html>
<html>
<head>
	<title>welcome to hirefreehands</title>
</head>
<style>
        
        @import url('https://fonts.googleapis.com/css?family=Ubuntu');
        
        html body {
            font-family: 'Ubuntu', sans-serif;
            background: #efefef !important;
            width: 100%; 
        }

        h1{
            font-size: 3.3em;
            color: #0BC773;
        }
        
        h2 {
            font-size: 1.2em;
            color: #fff;
        }
        
        .btn{
            padding: 16px 25px;
            border-radius: 12px;
            border-style: solid;
            border-color: #fff;
            text-decoration: none;
            
        }
    
        .footer {
            text-align: center;
        }
        
        .footer a {
            display: inline;
            text-decoration: none;
        }
        
        a {
            text-decoration: none;
        }
        
        p {
            color: #111;
            font-weight: 100;
            font-size: 1em;
        }

    </style>
<body>
<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background: #fff; padding: 70px;">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#">
                                    <img style="height: 25px;padding-top: 0px;" src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0" style="margin-top:2em;">
                        <tr>
                            <td style="width: 70%;">
                                <h1 style="font-size: 2em; color: #0BC773; margin-top: 5px;"> We’re Thrilled to Have You, {{ $user->firstname }}</h1>
                                
                            </td>
                             <td style="display: inline; width: 30%;">
                                <img style="width: 80%;" src="https://s26.postimg.org/w9czsqluh/Welcome-_Illustration_1.png">
                            </td> 
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <!-- call to action-->
                <td style="border-spacing: 1em;">
                    <p style="padding-bottom:23px; color: #111; font-size: 1em;">
                       You’ve just joined hundreds of startups & companies who get top-quality work done through expert, hand-picked, digital freelancers on our platform. On Hirefreehands, you can be assured of world-class talent with exceptional customer service at the perfect price. Our freehands specialize across these categories:
                    </p>
                </td>
            </tr>
        </table>

         <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding: 0px;">
            <tr style="border-spacing: 3em;"> <!-- call to action-->
                <td style="padding: 20px 70px; background-color: #0AC775">
                  
                    <img style="width: 100%;padding-top: 3px; background-color: #0BC773;" src=" https://s26.postimg.org/3y2oqyezd/categories.png" >
                    <br>
                    <br><br>                 
                    <a href="https://hirefreehands.tech/jobs" class="btn" style="text-decoration: none; border-radius: 6px; background-color: #fff; border-color: #fff; margin: 32%;">
                        <b style="color: #0AC775; text-align: center;">
                            Get Started
                        </b>
                    </a>
                    <p style="font-size: 1em; color: #fff; padding-top: 20px;">
                    Have a lovely day,<br>

                    The Hirefreehands Team.
                    </p>
                </td>
            </tr><!--end here-->
		</table>
		<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding-top: 40px;">
            <tr>
                <td class="footer" style="text-align: center;">

                    <tr>
                        <td style="text-align:center">
                            <a href="https://www.facebook.com/hirefreehands/">
                                <img style="margin: 0px;" height="16px" src="https://s30.postimg.org/66nv9rf35/facebook.png">
                            </a>
                            <a href="https://twitter.com/hirefreehands">
                                <img style="margin: 0px; margin-left:20px;" height="20px" src="https://s30.postimg.org/ppsgj4dup/twitter.png">
                            </a>
                            <a href="https://www.linkedin.com/company-beta/10832378/">
                                <img style="margin: 0px; margin-left:20px;" height="16px" src="https://s30.postimg.org/rvmrdmhb5/linkedin.png"><br/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center">
                            <img src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png" height="30" style="margin: 10px; padding-top: 26px;">

                            <p style="margin-bottom: 40px;  font-size: 10px; color: #999;">Need any help? Get in touch with us on <a href="mailto:help@hirefreehands.tech" target="_top">help@hirefreehands.tech</a>
                                <br>&copy; Hirefreehands 2017. All Rights Reserved</p>
                        </td>
                    </tr>
                </td>
            </tr>
        </table>
    </center>
</body>
