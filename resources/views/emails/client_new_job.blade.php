<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>Client</title>
    <style>
        
        @import url('https://fonts.googleapis.com/css?family=Ubuntu');
        
        html body {
            font-family: 'Ubuntu', sans-serif;
            background: #efefef !important;
            width: 100%; 
        }

        h1{
            font-size: 2em;
            color: #0BC773;
        }
        
        h2 {
            font-size: 1.2em;
            color: #fff;
        }
        
       .btn {
            padding: 12px 24px;
            border-radius: 6px;
            background-color: #0AC775;
        }
    
        .footer {
            text-align: center;
        }
        
        .footer a {
            display: inline;
            text-decoration: none;
        }
        
        a {
            text-decoration: none;  
        }
        
        p {
            color: #111;
            font-size: 1em;
        }
        
         @media only screen and (max-device-width: 780px){
          .mobile {
              padding-top: 35px;
            }
        }

    </style>
</head>
<body style="background: #efefef !important;  padding-top: 50px !important;">
    <center>


	<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background: #fff; padding: 70px;">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                           
                            <td>
                                <a href="#">
                                    <img style="height: 25px;padding-top: 0px;" src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table  class="mobile" border="0" style="padding-top: 5px;">
                        
                        <tr>
                            <td style="width: 70%;">
                                <h1 style="font-size: 2em;color: #0BC773;">{{ $job->title }}</h1>
                            </td>
                            <td style="display: inline; width: 30%;">
                                <img style="width: 100%;" src="https://s26.postimg.org/4zq4s7rmh/Bid-_Approved.png">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> 
        
        <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding: 0px;">
            <tr style="border-spacing: 3em;"> <!-- call to action-->
                <td style="padding: 20px 70px; background-color: #333">
                    
                    <p style="padding: 10px  0; font-size: 1em; color: #fff;">
                        Thanks for posting your job on Hirefreehands. You’re in for a treat! Here’s what’s next:
                    </p>
                    <ul style="font-size: 1em; color: #fff; padding: 5px 20px;">
                        <li>  
                            <p style="color: #fff;font-size: 1em;"> 
                                A member of our team will get in touch with you shortly. This wonderful person will be on hand to render any assistance you need on this job - from helping you make freelancer recommendations, to guiding your communication with the freelancer and explaining how to manage your freelancer’s delivery.
                            </p>   
                        </li>
                        <li>
                            <p style="color: #fff;font-size: 1em;">
                                You will start receiving bids shortly from qualified freehands who are interested in your job according to the specifications you have set (delivery time, price, add-ons etc). Most bids usually arrive within the first few hours of posting. 

                            </p>
                        </li>

                        <li>  
                            <p style=" color: #fff;font-size: 1em;">
                                You can review the freelancers’ proposals / bids. You can also ask them questions and view their profiles on the platform, to see their qualifications and view their portfolios where applicable. Doing this helps you make a better pick. 

                            </p>   
                        </li>
                        
                    </ul>

                    <a href="hirefreehands.tech/jobs" class="btn" style="text-decoration: none; padding: 12px 24px;border-radius: 6px; background-color:#0AC775"><b style="color: #fff">Go to Job</b></a>

                    <p style="padding: 20px 0; color: #fff;font-size: 1em; font-weight: bold;">
                        PS - Exchanging of personal information with freelancers on the platform is prohibited and violates our <a href="https://hirefreehands.tech/legal"> Terms of Service. </a> To ensure that   all users are protected, we request that all communication on jobs be kept within the platform, or relayed through customer support personnel where applicable.
                    </p>

                    <p style=" color: #fff;font-size: 1em;">Have a lovely day,<br>The Hirefreehands Team.</p>
                </td>
            </tr>
        </table>
            <!--end here-->
			<table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding-top: 40px;">
            <tr>
                <td class="footer" style="text-align: center;">

                    <tr>
                        <td style="text-align:center">
                            <a href="https://www.facebook.com/hirefreehands/">
                                <img style="margin: 0px;" height="16px" src="https://s30.postimg.org/66nv9rf35/facebook.png">
                            </a>
                            <a href="https://twitter.com/hirefreehands">
                                <img style="margin: 0px; margin-left:20px;" height="20px" src="https://s30.postimg.org/ppsgj4dup/twitter.png">
                            </a>
                            <a href="https://www.linkedin.com/company-beta/10832378/">
                                <img style="margin: 0px; margin-left:20px;" height="16px" src="https://s30.postimg.org/rvmrdmhb5/linkedin.png"><br/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center">
                            <img src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png" height="30" style="margin: 10px; padding-top: 26px;">

                            <p style="margin-bottom: 40px;  font-size: 10px; color: #999;">Need any help? Get in touch with us on <a href="mailto:help@hirefreehands.tech" target="_top">help@hirefreehands.tech</a>
                                <br>&copy; Hirefreehands 2017. All Rights Reserved</p>
                        </td>
                    </tr>
                </td>
            </tr>
        </table>
    </center>
</body>

</html>