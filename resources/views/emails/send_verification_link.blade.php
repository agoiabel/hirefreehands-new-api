<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no"> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>Verify Email</title>
    <style>
        
        @import url('https://fonts.googleapis.com/css?family=Ubuntu');
        
        html body {
            font-family: 'Ubuntu', sans-serif;
            background: #efefef !important;
            width: 100%; 
        }

        h1{
            font-size: 3.3em;
            color: #0BC773;
        }
        
        h2 {
            font-size: 1.2em;
            color: #fff;
        }
        
        .btn{
            padding: 16px 25px;
            border-radius: 12px;
            border-style: solid;
            border-color: #fff;
            text-decoration: none;
            
        }
    
        .footer {
            text-align: center;
        }
        
        .footer a {
            display: inline;
            text-decoration: none;
        }
        
        a {
            text-decoration: none;
        }
        
        p {
            color: #111;
            font-weight: 100;
            font-size: 1em;
        }

    </style>
</head>
<body style="background: #efefef !important;  padding-top: 50px !important;">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background: #fff; padding: 70px;">
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#">
                                    <img style="height: 25px;padding-top: 0px;" src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png">
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table border="0" style="margin-top:3em;">
                        
                        <tr>
                            <td style="width: 70%;">
                                <h1 style="font-size: 3.2em;color: #0BC773;"> Email Address Verification</h1>

                            </td>
                            <td style="display: inline; margin: 0px; width: 30%;">
                                <img style="width: 100%; margin-top: 40px;" src="https://s27.postimg.org/qk3h169zj/verify-e_Mail.png">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> 
        
        <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding: 0px;">
            <tr style="border-spacing: 3em;"> <!-- call to action-->
                <td style="padding: 20px 70px; background-color: #0AC775">
                    <h2 style="color: #fff; margin-top:50px; padding-bottom:20px;">Hi {{ $user->firstname }},</h2>
                    <p style="font-size: 1em; color: #fff; padding-bottom: 40px;font-weight: 100;">Thanks for registering on Hirefreehands. To confirm your email address and activate your account, please click the link below:</p>
        
                    <a href='http://localhost:3000/verify/{{$user->token}}' class="btn" style="padding: 16px 25px;border-radius: 12px;border:2px solid #fff;  text-decoration: none;"><b style="color: #fff; font-size: 1.1em;">Confirm Email</b></a>
                    <p style="font-size: 14px; color: #fff; padding-top: 40px;font-weight: 100;font-size: 1em;">If you did not register on Hirefreehands and got this email in error, kindly ignore. Someone probably just typed the wrong email address.<br><br>

                    Have a lovely day,<br>

                    The Hirefreehands Team.
                    </p>
                </td>
            </tr><!--end here-->
        </table>
         <table border="0" cellpadding="0" cellspacing="0" style="width:600px; background-color: #e8e8e8; padding-top: 40px;">
            <tr>
                <td class="footer" style="text-align: center;">

                    <tr>
                        <td style="text-align:center">
                            <a href="https://www.facebook.com/hirefreehands/">
                                <img style="margin: 0px;" height="16px" src="https://s30.postimg.org/66nv9rf35/facebook.png">
                            </a>
                            <a href="https://twitter.com/hirefreehands">
                                <img style="margin: 0px; margin-left:20px;" height="20px" src="https://s30.postimg.org/ppsgj4dup/twitter.png">
                            </a>
                            <a href="https://www.linkedin.com/company-beta/10832378/">
                                <img style="margin: 0px; margin-left:20px;" height="16px" src="https://s30.postimg.org/rvmrdmhb5/linkedin.png"><br/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center">
                            <img src="https://s30.postimg.org/hmua7st9d/HFH-_Logo_2x-no_Beta.png" height="30" style="margin: 10px; padding-top: 26px;">

                            <p style="margin-bottom: 40px;  font-size: 10px; color: #999;">Need any help? Get in touch with us on <a href="mailto:" target="_top">help@hirefreehands.tech</a>
                                <br>&copy; Hirefreehands 2017. All Rights Reserved</p>
                        </td>
                    </tr>
                </td>
            </tr>
        </table>
    </center>
    </body>
</html>