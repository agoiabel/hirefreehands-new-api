<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const EMAIL_VERIFIED = true;
    const EMAIL_NOT_VERIFIED = false;

    const ACTIVE = true;
    const INACTIVE = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'firstname', 'lastname', 'email', 'password', 'active', 'email_verified', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * a user has many roles
     * 
     * @return 
     */
    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class, 'user_id');
    }

    /**
     * Bcrypt the user password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    

}
