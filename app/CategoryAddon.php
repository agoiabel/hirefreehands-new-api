<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryAddon extends Model
{
    
    const toogle_off_milestone = 0;
    const toogle_on_milestone = 1;
 
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'category_id', 'title', 'toogle_on_milestone', 'weight', 'operator'
    ];

    /**
     * this belongs to a category
     * 
     * @return 
     */
    public function category()
    {
    	return $this->belongsTo(Category::class, 'category_id');
    }
    
}
