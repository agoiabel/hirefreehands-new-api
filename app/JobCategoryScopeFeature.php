<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategoryScopeFeature extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'feature_id', 'job_category_scope_id', 'price', 'no_of_days'
    ];

    /**
     * this belongs to feature
     * 
     * @return 
     */
    public function feature()
    {
    	return $this->belongsTo(Feature::class, 'feature_id');
    }

    /**
     * this belong to job_category
     * 
     * @return 
     */
    public function job_category_scope()
    {
    	return $this->belongsTo(JobCategoryScope::class, 'job_category_scope_id');
    }
    
}
