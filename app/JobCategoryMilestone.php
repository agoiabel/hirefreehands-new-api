<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategoryMilestone extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_category_id', 'title', 'description', 'weight', 'skill_group_id'
    ];

    /**
     * a jobCategoryMilestone belongs to a job category
     * 
     * @return 
     */
    public function job_category()
    {
    	return $this->belongsTo(JobCategory::class, 'job_category_id');
    }
}
