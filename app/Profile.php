<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	/**
	 * Attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'user_id', 'qualification', 'portfolio', 'about', 'address', 'city', 'state', 'gender', 'image', 'bank', 'bvn', 'account_number', 'phone_number'
    ];

    /**
     * A profile belongs to a user
     * 
     * @return 
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

}
