<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var array
	 */
    protected $fillable = [
    	'name', 'description', 'type', 'milestone_weight', 'banner', 'has_child_scope', 'require_only_one_freelancer'
    ];

    /**
     * a job category has many skills
     * 
     * @return 
     */
    public function skills()
    {
    	return $this->hasMany(Skill::class, 'job_category_id');
    }
        
    /**
     * a job category has many scopes
     * 
     * @return 
     */
    public function scopes()
    {
        return $this->hasMany(JobCategoryScope::class, 'job_category_id');       
    }

    /**
     * a job category has many milestones
     * 
     * @return 
     */
    public function milestones()
    {
        return $this->hasMany(JobCategoryMilestone::class, 'job_category_id');
    }

    /**
     * a category has many child_scopes
     * 
     * @return 
     */
    public function child_scopes()
    {
        return $this->hasMany(JobCategoryChildScope::class, 'job_category_id');
    }

    /**
     * this has many addons
     * 
     * @return 
     */
    public function addons()
    {
        return $this->hasMany(CategoryAddon::class, 'category_id');
    }
}
