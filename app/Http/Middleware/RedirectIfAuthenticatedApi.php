<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedApi
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( empty($request->header('Authorization')) ) {
            return response()->json([
                'message' => 'you need to be authenticated'
            ], 401);
        }

        try {
            Auth::login( $this->user->where('token', $request->header('Authorization'))->firstOrFail() );

            return $next($request);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'bad request, seems your token expired'
            ], 400);   
        }
    }
}
