<?php

namespace App\Http\Requests;

use App\User;
use App\Events\UserWasCreated;
use Illuminate\Foundation\Http\FormRequest;
use App\HelperClasses\Utilities\TokenGenerator;

class RegistrationFormRequest extends FormRequest
{
    public $user, $generate;

    /**
     * Inject classes that will be needed inside of this controller
     */
    public function __construct(User $user, TokenGenerator $generate)
    {
        $this->user = $user;

        $this->generate = $generate;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users',
            'firstname' => 'required',
            'lastname' => 'required',
            'password' => 'required',
        ];
    }

    /**
     * Handle the process of registration
     * 
     * @return 
     */
    public function handle()
    {
        //create new user
        $newUser = $this->createNewUserFrom($this);

        //an event handler that 
            //send email verification link, create user,s role and profile
        event(new UserWasCreated($newUser));
    }

    /**
     * Create new user
     * 
     * @return 
     */
    public function createNewUserFrom($newUser)
    {
        return $this->user->create([
            'uid' => $this->generate->token(),
            'email' => $newUser->email,
            'firstname' => $newUser->firstname,
            'lastname' => $newUser->lastname,
            'password' => $newUser->password,
            'token' => $this->verificationToken()
        ]);
    }

    /**
     * Handle the process of generating verification token
     * 
     * @return 
     */
    public function verificationToken()
    {
        return $this->generate->token();
    }

}
