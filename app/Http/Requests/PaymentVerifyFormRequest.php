<?php

namespace App\Http\Requests;

use App\Job;
use Carbon\Carbon;
use App\Transaction;
use App\JobMilestone;
use Illuminate\Foundation\Http\FormRequest;
use App\HelperClasses\Utilities\EstimateJobPrice;

class PaymentVerifyFormRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(JobMilestone $jobMilestone, Transaction $transaction, EstimateJobPrice $estimateJobPrice)
    {
        $this->transaction = $transaction;
        $this->jobMilestone = $jobMilestone;    
        $this->estimateJobPrice = $estimateJobPrice;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Handle the process verifying payment
     * 
     * @return 
     */
    public function handle()
    {
        $country_code = $this->country_code;
        $paymentResponse = $this->checkPaymentFrom($this->paystackResponse);
        
        $jobMilestone = $this->jobMilestone->with(['job'])->where('id', $this->job_milestone_id)->firstOrFail();

        $amount = $this->estimateJobPrice->getActualPriceFrom($jobMilestone->price, $country_code) * 100;

        if ( ($paymentResponse['data']['status'] == 'success') && ($amount == $paymentResponse['data']['amount']) ) {

            //update transaction with ref
            $this->transaction->where('transaction_id', $this->paystackResponse['reference'])->firstOrFail()->update([
                'status' => Transaction::VERIFIED
            ]);

            //update jobmilestone payment
            $jobMilestone->update([
                'paid_for' => JobMilestone::PAID,
            ]);

            //update job_current_milestone_id and job status
            //This should be done when freelancer signed

            $jobMilestone->job->update([
                'status' => Job::JOB_STARTED,
                'current_milestone_id' => $jobMilestone->id,
            ]);

            return response()->json([
                'message' => 'Transaction was successful'
            ], 200);            
        }    
        return response()->json([
            'message' => 'transaction was not successful'
        ], 422);    
    }


    protected function checkPaymentFrom($paystackResponse)
    {
        $result = array();
        $url = 'https://api.paystack.co/transaction/verify/'.$paystackResponse['reference'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
          $ch, CURLOPT_HTTPHEADER, [ #sk_live_50eb5406c976fc9e2c44b3e54ce1039bcf32b191
            'Authorization: Bearer sk_test_f272874046c3d74105fd13096cc86f0b606ed638'] #sk_test_f272874046c3d74105fd13096cc86f0b606ed638
        );
        $request = curl_exec($ch);
        curl_close($ch);

        if ($request) {
          return json_decode($request, true);
        }
        return false;
    }
}
