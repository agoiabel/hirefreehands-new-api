<?php

namespace App\Http\Requests;

use App\User;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\HelperClasses\Utilities\TokenGenerator as Generate;

class LoginFormRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(User $user, Generate $generate)
    {
        $this->user = $user;

        $this->generate = $generate;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
        ];
    }

    /**
     * Handle Processing of login
     * 
     * @return 
     */
    public function handle()
    {
        try {
            $user =  $this->user->with(['roleUsers.role'])->where('email', $this->email)->firstOrFail();

            //check if password inputted matches the user,s password 
            $this->checkIfUserPasswordIsCorrect($user);

            //check if user is active and verified
            $this->checkIfUserIsActiveAndVerified($user);

            $user->update([
                'token' => $this->generate->token()
            ]);

            return $user;
        } catch (ModelNotFoundException $e) {
            throw new CustomException(route('index'), "bad credentials, seems something is wrong with email");
        }
    }

    /**
     * Check if user,s password is correct
     * 
     * @return 
     */
    protected function checkIfUserPasswordIsCorrect($user)
    {
        if (! Hash::check($this->password, $user->password) ) {
            throw new CustomException(route('index'), "bad credentials, seems something is wrong with password");
        }
    }

    /**
     * Check if user is verified and active
     * 
     * @param  User $user
     * @return       
     */
    protected function checkIfUserIsActiveAndVerified($user)
    {
        //check if user is active and email verified 
        if (! $this->isActiveAndEmailVerified($user) ) {
            throw new CustomException(route('index'), "Oops, you are in active. contact admin");
        }

        //authenticate user
        Auth::login($user, true);
    }

    /**
     * Check if the user email is verified and if the user is active
     * 
     * @return boolean 
     */
    protected function isActiveAndEmailVerified($user)
    {
        if ($user->email_verified == User::EMAIL_VERIFIED && $user->active == User::ACTIVE) {
            return true;
        }

        return false;
    }

}
