<?php

namespace App\Http\Requests;

use App\Job;
use App\Transaction;
use App\JobMilestone;
use Illuminate\Foundation\Http\FormRequest;
use App\HelperClasses\Utilities\TokenGenerator;
use App\HelperClasses\Utilities\EstimateJobPrice;

class PaymentFormRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(Job $job, JobMilestone $jobMilestone, EstimateJobPrice $estimateJobPrice, Transaction $transaction)
    {
        $this->job = $job;

        $this->transaction = $transaction;

        $this->jobMilestone = $jobMilestone;

        $this->estimateJobPrice = $estimateJobPrice;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_id' => 'required',
            'country_code' => 'required',
        ];
    }

    /**
     * Handle the process 
     * 
     * @return 
     */
    public function handle()
    {
        $jobMilestone = $this->jobMilestone->with(['job'])->where('id', $this->job_milestone_id)->where('job_id', $this->job_id)->firstOrFail();

        $array = [];

        //initialize_transaction
        $array['transaction_id'] = $this->initialize_transaction_from($jobMilestone)->transaction_id;
        $array['price'] = $this->estimateJobPrice->getActualPriceFrom($jobMilestone->price, $this->country_code);
        $array['email'] = $jobMilestone->job->owner->email;

        return $array;
    }

    /**
     * Initialize transaction
     * 
     * @param $jobMilestone 
     * @return 
     */
    protected function initialize_transaction_from($jobMilestone)
    {
        return $this->transaction->create([
            'user_id' => $jobMilestone->job->owner->id,
            'transaction_id' => (new TokenGenerator())->token(),
            'details' => 'Paid for the job "'.$jobMilestone->job->title.'"',
            'amount' => $jobMilestone->price,
            'type' => 'CLIENT_START_JOB',
            'resource_id' => $jobMilestone->job_id
        ]);
    }
}