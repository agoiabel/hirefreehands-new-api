<?php

namespace App\Http\Requests;

use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Foundation\Http\FormRequest;

class SendTokenFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code' => 'required',
            'number' => 'required'
        ];
    }

    /**
     * handle the logic for sending the sms
     * 
     * @return 
     */
    public function handle()
    {
        $phone_number = $this->country_code.$this->number;
        $token = $this->generateRandomNumber();
        $nexmo = $this->sendSmsFrom($phone_number, $token);

        $array = [];
        $array['phone_number'] = $phone_number;
        $array['nexmo'] = $nexmo;
        $array['token'] = $token;

        return $array;
    }

    /**
     * send sms
     * 
     * @param $phone_number 
     * @param $token        
     * 
     * @return               
     */
    protected function sendSmsFrom($phone_number, $token)
    {
        return Nexmo::message()->send([
                'to' => $phone_number,
                'from' => '12064792234',
                'text' => $token
        ]);
    }

    /**
     * Generate random unique number
     * 
     * @return 
     */
    protected function generateRandomNumber()
    {
        return mt_rand(1000,9999);
    }


}
