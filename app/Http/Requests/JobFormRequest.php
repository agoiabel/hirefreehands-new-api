<?php

namespace App\Http\Requests;

use App\Job;
use App\Events\JobWasCreated;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Http\FormRequest;

class JobFormRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(Job $job, Guard $auth)
    {
        $this->job = $job;

        $this->auth = $auth;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // ''
        ];
    }

    /**
     * Handle the process of storing new job
     * 
     * @return 
     */
    public function handle()
    {
        $job = $this->createNewJob();

        event(new JobWasCreated($job, $this));

        return $job;
    }

    /**
     * Handle the creation of new job
     * 
     * @return 
     */
    protected function createNewJob()
    {
        return $this->job->create([
            'title' => $this->title,
            'client_id' => $this->auth->user()->id,
            'job_category_id' => $this->job_category_id,
            'description' => $this->description,
            'price' => $this->price,
            'hasMilestones' => $this->hasMilestones,
            'job_category_scope_id' => $this->job_category_scope_id,
            'scope_duration_point_id' => $this->scope_duration_point_id,
            'coupon_id' => $this->coupon_id,
            'discount' => $this->discount,
            'job_category_child_scope_id' => $this->job_category_child_scope_id,
        ]);
    }

}
