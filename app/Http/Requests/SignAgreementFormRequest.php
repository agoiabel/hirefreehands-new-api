<?php

namespace App\Http\Requests;

use App\Job;
use Carbon\Carbon;
use App\JobMilestone;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Mail;
use App\Mail\ClientSignedJobAgreement;
use Illuminate\Foundation\Http\FormRequest;

class SignAgreementFormRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(Job $job, Guard $auth, JobMilestone $jobMilestone)
    {
        $this->job = $job;    

        $this->auth = $auth;

        $this->jobMilestone = $jobMilestone;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * handle the process of signing new agreement
     * 
     * @return 
     */
    public function handle()
    {
        $job = $this->job->where('id', $this->job_id)->firstOrFail();
        $jobMilestone = $this->jobMilestone->where('id', $job->id)->firstOrFail();

        if ($this->type == "client") {

            if (is_null($job->current_milestone_id)) {
                $jobMilestone->update([
                    'client_signed' => JobMilestone::SIGNED
                ]);
            }

            //sign agreement
            Mail::to($this->auth->user())->send(new ClientSignedJobAgreement($job, $this->auth->user()));

            return $jobMilestone;
        }

        //freelancer signed, add date and update job starts
        $jobMilestone->freelancer->update([
            'freelancer_signed' => JobMilestone::SIGNED
        ]);
        return $jobMilestone->job->update([
            'job_start_date' => Carbon::now(),
            'job_end_date' => Carbon::now()->addDays($job->scope_point_duration->day)
        ]);

        //sign mail to client
    }

}
