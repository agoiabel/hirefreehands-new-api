<?php

namespace App\Http\Requests;

use App\Job;
use App\JobMilestone;
use Illuminate\Foundation\Http\FormRequest;

class JobSeenByClientOrFreelancerRequest extends FormRequest
{

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(Job $job, JobMilestone $jobMilestone)
    {
        $this->job = $job;

        $this->jobMilestone = $jobMilestone;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Handle the process of updating job
     * 
     * @return 
     */
    public function handle()
    {
        $job = $this->job->where('slug', $this->job_slug)->firstOrFail();
        $job_milestone = $this->jobMilestone->with(['freelancer'])->where('id', $job->current_milestone_id)->firstOrFail();

        if ($this->job_user == 'client') {
            return $job_milestone->update([
                'client_seen' => true
            ]);
        } 

        return $job_milestone->freelancer->update([
            'freelancer_seen' => true
        ]);
    }

}
