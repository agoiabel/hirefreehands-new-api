<?php

namespace App\Http\Requests;

use App\Job;
use App\User;
use App\JobMilestone;
use App\Mail\FreelancerBidAccepted;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Http\FormRequest;

class SetFreelancerFormRequest extends FormRequest
{

    public $job, $user, $jobMilestone;

    /**
     * Inject new needed classes into this file
     * 
     * @return void
     */
    public function __construct(JobMilestone $jobMilestone, User $user, Job $job)
    {
        $this->job = $job;

        $this->user = $user;

        $this->jobMilestone = $jobMilestone;    
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ''
        ];
    }

    /**
     * handle the process of setting freelancer
     * 
     * @return 
     */
    public function handle()
    {
        foreach ($this->jobMilestone->where('job_id', $this->job_id)->get() as $jobMilestone) {
            $jobMilestone->freelancer->update([
                'freelancer_id' => $this->freelancer_id
            ]);
        }

        $this->sendMailToFreelancer();
    }

    /**
     * Send message to freelancer
     * 
     * @return 
     */
    protected function sendMailToFreelancer()
    {
        $job = $this->job->where('id', $this->job_id)->firstOrFail();
        $freelancer = $this->user->where('id', $this->freelancer_id)->firstOrFail();

        Mail::to($freelancer)->send(new FreelancerBidAccepted($job, $freelancer));
    }
}
