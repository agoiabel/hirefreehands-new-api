<?php

namespace App\Http\Controllers;

use App\JobCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	/**
	 * Inject new needed classes into this file
	 * 
	 * @return void
	 */
	public function __construct(JobCategory $jobCategory)
	{
		$this->jobCategory = $jobCategory;
	}

	/**
	 * Return the list of categories with it,s relationships
	 * 
	 * @return 
	 */
    public function index(Request $request)
    {
		return response()->json([
				'categories' => $this->jobCategory->with([
					'milestones', 'child_scopes', 'scopes', 'scopes.scope_duration_points.duration', 'scopes.job_category_scope_addons.category_addon' 
			])->get()
		]);
    }

    /**
     * Get a single category
     * 
     * @param  $category_id 
     * @return              
     */
    public function show($category_id)
    {
    	return response()->json([
    		'category' => $this->jobCategory->with([
    			'scopes.scope_features.feature', 'scopes.scope_features.job_category_scope', 'scopes.scope_duration_points.duration'
    		])->where('id', $category_id)->firstOrFail()
    	]);
    }
}
