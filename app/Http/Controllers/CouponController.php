<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
	/**
	 * get all active coupons
	 * 
	 * @return 
	 */
    public function index(Coupon $coupon)
    {
    	return response()->json([
    		'coupons' => $coupon->where('status', Coupon::ACTIVE)->get()
    	]);
    }
}
