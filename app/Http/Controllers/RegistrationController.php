<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Mail\SendWelcomeMessage;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RegistrationFormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/*
|--------------------------------------------------------------------------
| Registration Controller
|--------------------------------------------------------------------------
|
| Here is where you can handle the registration and verification of new user
|
*/

class RegistrationController extends Controller
{
	/**
	 * Inject new needed classes into this file
	 * 
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * Handle the process of authenticatng new user
	 * with the RegistrationFromRequest as the helper class
	 * 
	 * @return 
	 */
    public function store(RegistrationFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'please check your inbox for verification'
    	], 200);
    }

    /**
     * Handle the process of verifying new user
     * 
     * @param string $verification_string 
     * @return 
     */
    public function verifyEmail($verification_string)
    {
		try {
			$user = $this->user->where('token', $verification_string)->firstOrFail();

			$this->updateUserFrom($user);

			Mail::to($user)->send(new SendWelcomeMessage($user));

			return response()->json([
				'message' => 'we successfully verified your email'
			], 200);

		} catch (ModelNotFoundException $e) {
	    	return response()->json([
	    		'message' => 'oops, verification_string not valid'
	    	], 401);		
		}
    }

    /**
     * Handle the process of updating user data when email is successfully verified
     * 
     * @param $user 
     * @return
     */
    protected function updateUserFrom($user)
    {
		$user->update([
			'token' => '',
			'active' => User::ACTIVE,
			'email_verified' => User::EMAIL_VERIFIED,
		]);
    }

}
