<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PaymentFormRequest;
use App\Http\Requests\PaymentVerifyFormRequest;
use App\Http\Requests\SignAgreementFormRequest;

class PaymentController extends Controller
{

    /**
     * Handle the process signing new agreement
     * 
     * @return 
     */
    public function sign_agreement(SignAgreementFormRequest $request)
    {
        $jobMilestone = $request->handle();

        return response()->json([
            'jobMilestone' => $jobMilestone
        ]);
    }

	/**
	 * initial payment
	 * 
	 * @return 
	 */
    public function initialize(PaymentFormRequest $request)
    {
    	$array = $request->handle();

    	return response()->json([
            'data' => [
        		'amount_in_kobo' => $array['price'] * 100,
        		'ref' => $array['transaction_id'],
        		'email' => $array['email']
    	   ]
        ]);
    }

    /**
     * Handle the process of verifying payment
     * 
     * @return 
     */
    public function verify(PaymentVerifyFormRequest $request)
    {
        return $request->handle();
    }
    
}
