<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use App\Http\Requests\LoginFormRequest;

class AuthenticationController extends Controller
{
	/**
	 * Handle authentication of user
	 * 
	 * @return 
	 */
    public function store(LoginFormRequest $request)
    {
        try {
    		$user = $request->handle();
            return response()->json([
                'user' => $user
            ], 200);
        } catch (CustomException $e) {
            return response()->json(['error' => 'oops, credentials not correct, check email and password'], 422);
        }
    }
    
}
