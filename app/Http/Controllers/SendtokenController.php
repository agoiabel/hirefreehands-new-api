<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SendTokenFormRequest;

class SendtokenController extends Controller
{
	/**
	 * Handle the process of sending sms to user,s phone number
	 * 
	 * @param  SendTokenFormRequest $request 
	 * @return 
	 */
    public function store(SendTokenFormRequest $request)
    {
    	$response = $request->handle();

    	return response()->json([ 'data' => [
    		'message' => 'phone number sent successfully',
    		'token' => $response['token'],
    		'nexmo' => $response['nexmo'],
    		'phone_number' => $response['phone_number']
    	]]);
    }
}
