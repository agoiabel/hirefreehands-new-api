<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SetFreelancerFormRequest;

class FreelancerController extends Controller
{
	/**
	 * Set Job Freelancer
	 * 
	 * @param Request $request
	 */
    public function set_single_freelancer(SetFreelancerFormRequest $request)
    {
    	//todo:: job_log
    	$request->handle();
    }
}
