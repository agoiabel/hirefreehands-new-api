<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use App\Http\Requests\JobFormRequest;
use App\Http\Requests\JobSeenByClientOrFreelancerRequest;

class JobController extends Controller
{
    /**
     * Get all jobs thats not ended
     * 
     * @return 
     */
    public function index(Job $job)
    {
        return response()->json([
            'jobs' => $job->with([
                'category',
                'scope',
                'milestones', 
                'freelancers', 
                'addons.addon',
                'owner'
            ])
            ->where('status', '!=', Job::JOB_ENDED)
            ->get()
        ]);
    }

	/**
	 * Handle the process of storing new job
	 * 
	 * @return 
	 */
    public function store(JobFormRequest $request)
    {        
    	$request->handle();

    	return response()->json(['message' => 'job was created successfully']);
    }

    /**
     * handle the process of getting a single job
     * 
     * @return 
     */
    public function show(Job $job, $job_slug)
    {
        return response()->json([
            'job' => $job->with([
                'category.milestones',
                'scope',
                'milestones.freelancer', 
                'freelancers', 
                'addons.addon',
                'owner',
                'scope_point_duration.duration'
            ])->where('slug', $job_slug)->firstOrFail()
        ]);
    }

    /**
     * Handle the process of updating job status
     * 
     * @param  Request $request 
     * @return            
     */
    public function seen(JobSeenByClientOrFreelancerRequest $request)
    {
        $request->handle();

        return response()->json([
            'message' => 'updated successfully'
        ]);
    }
}