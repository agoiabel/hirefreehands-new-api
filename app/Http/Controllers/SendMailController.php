<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
	/**
	 * Handle the process of sending mail
	 * 
	 * @param  Request $request 
	 * @return            
	 */
    public function store(Request $request)
    {
		Mail::to($user)->send(new MailSenderHelper($request));
    }
}
