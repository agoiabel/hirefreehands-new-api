<?php

namespace App\Listeners;

use App\Events\JobWasCreated;
use App\Mail\ClientJobCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewJobCreatedMailToClient
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated  $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        Mail::to($this->auth->user())->send(new ClientJobCreated($event->job, $this->auth->user()));
    }
}
