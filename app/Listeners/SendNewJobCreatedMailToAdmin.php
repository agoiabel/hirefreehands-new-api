<?php

namespace App\Listeners;

use App\Events\JobWasCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\NotifyAdminClientJobCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewJobCreatedMailToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated  $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        Mail::to($this->auth->user())->send(new NotifyAdminClientJobCreated($event->job, $this->auth->user()));
    }
}
