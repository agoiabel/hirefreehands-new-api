<?php

namespace App\Listeners;

use App\JobMilestone;
use App\JobFreelancer;
use App\Events\JobWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HandleMilestoneCase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(JobMilestone $jobMilestone, JobFreelancer $jobFreelancer)
    {
        $this->jobMilestone = $jobMilestone;

        $this->jobFreelancer = $jobFreelancer;
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated  $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        //check if milestone is clicked, if no
        if (! $event->job->hasMilestones) {

            //store
            $jobMilestone = $this->jobMilestone->create([
                'job_id' => $event->job->id,
                'price' =>  $event->job->price
            ]);

            //save jobMilestone
            $this->jobFreelancer->create([
                'job_id' => $event->job->id,
                'job_milestone_id' => $jobMilestone->id,
                // 'skill_group_id' => $milestone->skill_group_id,
            ]);

            return true;
        }

        // $job_actual_price_without_hirefreehands

        foreach ($event->job->category->milestones as $milestone) {
            //store
            $jobMilestone = $this->jobMilestone->create([
                'job_id' => $event->job->id,
                'job_category_milestone_id' => $milestone->id,
                'price' =>  $event->job->price * ($milestone->weight / 100)
            ]);

            //save jobMilestone
            $this->jobFreelancer->create([
                'job_id' => $event->job->id,
                'job_milestone_id' => $jobMilestone->id,
                'skill_group_id' => $milestone->skill_group_id,
            ]);
        }
    }
}
