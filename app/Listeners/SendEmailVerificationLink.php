<?php

namespace App\Listeners;

use App\Events\UserWasCreated;
use App\Mail\SendVerificationLink;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailVerificationLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        $this->sendVerificationLinkFor($event->user);
    }


    /**
     * Send verification link for new user
     * 
     * @param $newUser 
     * @return          
     */
    public function sendVerificationLinkFor($newUser)
    {
        Mail::to($newUser)->send(new SendVerificationLink($newUser));   
    }


}
