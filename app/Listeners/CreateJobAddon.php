<?php

namespace App\Listeners;

use App\JobAddon;
use App\Events\JobWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateJobAddon
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(JobAddon $jobAddon)
    {
        $this->jobAddon = $jobAddon;   
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated  $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        foreach ($event->jobFormRequest->addons as $addon) {
            $this->jobAddon->create([
                'job_id' => $event->job->id,
                'job_category_scope_addon_id' => $addon,
            ]);
        }
    }
}
