<?php

namespace App\Listeners;

use App\Role;
use App\RoleUser;
use App\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateNewUserRole
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RoleUser $roleUser)
    {
        $this->roleUser = $roleUser;
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        $this->roleUser->create([
            'user_id' => $event->user->id,
            'role_id' => Role::CLIENT
        ]);
    }
}
