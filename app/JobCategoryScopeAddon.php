<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategoryScopeAddon extends Model
{
    const toogle_off_milestone = 0;
    const toogle_on_milestone = 1;
    
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_category_scope_id', 'category_addon_id'
    ];

    /**
     * a jobCategoryScopeAddon belongs to a jobCategoryScope
     *  
     * @return 
     */
    public function job_category_scope()
    {
    	return $this->belongsTo(JobCategoryScope::class, 'job_category_scope_id');	
    }

    /**
     * this belongs to CategoryAddon
     * 
     * @return 
     */
    public function category_addon()
    {
        return $this->belongsTo(CategoryAddon::class, 'category_addon_id');
    }

    /**
     * a jobCategoryScopeAddon has many jobAddons
     * 
     * @return 
     */
    public function jobAddons()
    {
        return $this->hasMany(JobAddon::class, 'job_category_scope_addon_id');
    }
    
}
