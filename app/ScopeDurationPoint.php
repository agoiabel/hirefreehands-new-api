<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScopeDurationPoint extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'duration_id', 'job_category_scope_id', 'day', 'price'
    ];

    /**
     * a scope_duration_point belongs to a job_category_scope
     * 
     * @return 
     */
    public function job_category_scope()
    {
    	return $this->belongsTo(JobCategoryScope::class, 'job_category_scope_id');
    }

    /**
     * a scope_duration_point belongs to a duration
     * 
     * @return 
     */
    public function duration()
    {
    	return $this->belongsTo(Duration::class, 'duration_id');
    }
}