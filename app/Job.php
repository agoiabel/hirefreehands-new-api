<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HelperClasses\Utilities\SluggableTrait;

class Job extends Model
{
    use SluggableTrait;

	const CLIENT_NOT_PAID = false;
	const CLIENT_PAID = true;

	const JOB_NOT_STARTED = 0; 
	const JOB_STARTED = 1; 
	const JOB_ENDED = 2; 

    const JOB_NOT_SIGNED = false;
    const JOB_SIGNED = true;

    /**
     * Generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->title;
    }

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_category_id', 'coupon_id', 'scope_duration_point_id', 'job_category_scope_id', 'client_id', 'job_category_child_scope_id', 'job_start_date', 'job_end_date',
        'project_manager_id', 'client_paid', 'status', 'discount', 'title', 'description', 'hasMilestones', 'price', 'current_milestone_id', 'slug'
    ];

    /**
     * a job belongs to a category
     * 
     * @return 
     */
    public function category()
    {
        return $this->belongsTo(JobCategory::class, 'job_category_id');
    }

    /**
     * a job belongs to an owner
     * 
     * @return 
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    /**
     * a job belongs to a project manager
     * 
     * @return 
     */
    public function project_manager()
    {
    	return $this->belongsTo(User::class, 'project_manager_id');
    }

    /**
     * a job belongs to a duration
     * 
     * @return 
     */
    public function scope_point_duration()
    {
    	return $this->belongsTo(ScopeDurationPoint::class, 'scope_duration_point_id');
    }

    /**
     * a job belongs to a coupon
     * 
     * @return 
     */
    public function coupon()
    {
    	return $this->belongsTo(Coupon::class, 'coupon_id');
    }

    /**
     * a job belongs to a job_scope
     * 
     * @return 
     */
    public function scope()
    {
    	return $this->belongsTo(JobCategoryScope::class, 'job_category_scope_id');
    }

    /**
     * a job belongs to a child_scope
     * 
     * @return 
     */
    public function child_scope()
    {
    	return $this->belongsTo(JobCategoryChildScope::class, 'job_category_child_scope_id');
    }

    /**
     * a job has many jobFreelancers
     * 
     * @return 
     */
    public function freelancers()
    {
    	return $this->hasMany(JobFreelancer::class, 'job_id');
    }

    /**
     * a job has many addons
     * 
     * @return 
     */
    public function addons()
    {
    	return $this->hasMany(JobAddon::class, 'job_id');
    }

    /**
     * a job has many milestones
     * 
     * @return 
     */
    public function milestones()
    {
    	return $this->hasMany(JobMilestone::class, 'job_id');
    }

    /**
     * a job has many logs
     * 
     * @return 
     */
    public function logs()
    {
    	return $this->hasMany(JobLog::class, 'job_id');
    }

    /**
     * a job has many reviews
     * 
     * @return 
     */
    public function reviews()
    {
    	return $this->hasMany(JobReview::class, 'job_id');
    }


}
