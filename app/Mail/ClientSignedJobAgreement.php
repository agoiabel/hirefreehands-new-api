<?php

namespace App\Mail;

use App\Job;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientSignedJobAgreement extends Mailable
{
    use Queueable, SerializesModels;

    public $job, $client;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $client)
    {
        $this->job = $job;

        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.client_signed_job_agreement');
    }
}
