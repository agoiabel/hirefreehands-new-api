<?php

namespace App\Mail;

use App\Job;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientJobCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $job, $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $user)
    {
        $this->job = $job;
        
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.client_new_job');
    }
}
