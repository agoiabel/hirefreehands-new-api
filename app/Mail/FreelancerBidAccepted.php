<?php

namespace App\Mail;

use App\Job;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FreelancerBidAccepted extends Mailable
{
    use Queueable, SerializesModels;

    public $job, $freelancer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $freelancer)
    {
        $this->job = $job;

        $this->freelancer = $freelancer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.freelancer_bid_accepted');
    }
}