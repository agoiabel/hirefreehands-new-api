<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
	const ACTIVE = true;
	const INACTIVE = false;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['user_id', 'role_id', 'is_active'];

    /**
     * a roleUser belongs to a user
     * 
     * @return 
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * a roleUser belongs to a role
     * 
     * @return 
     */
    public function role()
    {
    	return $this->belongsTo(Role::class, 'role_id');
    }

}
