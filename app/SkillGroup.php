<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkillGroup extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name'
    ];

    /**
     * this has many JobCategoryMilestones 
     * 
     * @return 
     */
    public function job_category_milestones()
    {
    	return $this->hasMany(JobCategoryMilestone::class, 'skill_group_id');
    }

    /**
     * this has many skills
     * 
     * @return 
     */
    public function skills()
    {
    	return $this->hasMany(Skill::class, 'skill_group_id');
    }

    
}
