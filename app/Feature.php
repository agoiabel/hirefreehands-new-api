<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
	const BACKEND = 1; 
	const FRONTEND = 2; 
	const FRONTEND_BACKEND = 3; 

	const ACTIVE = true;
	const INACTIVE = false;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name', 'status',
    ];

    /**
     * a feature has many job category scope
     * 
     * @return 
     */
    public function job_category_scope_features()
    {
    	return $this->hasMany(JobCategoryScopeFeature::class, 'feature_id');	
    }

}
