<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAddon extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_id', 'job_category_scope_addon_id'
    ];

    /**
     * a jobAddon belongs to a job
     * 
     * @return 
     */
    public function job()
    {
    	return $this->belongsTo(Job::class, 'job_id');
    }

    /**
     * a jobAddon belongs to an addon
     * 
     * @return 
     */
    public function addon()
    {
    	return $this->belongsTo(JobCategoryScopeAddon::class, 'job_category_scope_addon_id');
    }
}
