<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobMilestone extends Model
{
	const UNCOMPLETED = false;
	const COMPLETED = true;

  const SIGNED = true;
  const NOT_SIGN = false;

	const PAID = true;
	const UNPAID = false;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
   	protected $fillable = [
   		'job_id', 'job_category_milestone_id', 'status', 'paid_for', 'client_signed', 'price', 'job_start_date', 'job_end_date', 'client_seen'
   	];

   	/**
   	 * a jobMilestone belongs to a job
   	 * 
   	 * @return 
   	 */
   	public function job()
   	{
   		return $this->belongsTo(Job::class, 'job_id');
   	}

   	/**
   	 * a jobMilestone belongs to a milestone
   	 * 
   	 * @return 
   	 */
   	public function milestone()
   	{
   		return $this->belongsTo(JobCategoryMilestone::class, 'job_category_milestone_id');
   	}

    /**
     * this has one job_freelancer
     * 
     * @return 
     */
    public function freelancer()
    {
       return $this->hasOne(JobFreelancer::class, 'job_milestone_id');
    }
      
}
