<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{	
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'title', 'job_category_id'
    ];

    /**
     * a skill belongs to a job category
     * 
     * @return 
     */
    public function job_category()
    {
    	return $this->belongsTo(JobCategory::class, 'job_category_id');
    }
    
}
