<?php 

namespace App\HelperClasses\Utilities;

trait SluggableTrait
{
    /**
     * Generate slug from model_id and parameter passed to the getSluggableString method
     * 
     * @return 
     */
    public static function boot()
    {
        static::created(function ($model) {
                $slug = $model->getSluggableString();

                $make_slug = $model->id . '-' . $slug;

                $model->slug = str_slug($make_slug);

                $model->save();
            });

        static::updating(function ($model) {
                $slug = $model->getSluggableString();
                
                $make_slug = $model->id . '-' . $slug;

                $model->slug = str_slug($make_slug);
            });
    }
}