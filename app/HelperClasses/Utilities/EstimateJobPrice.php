<?php 

namespace App\HelperClasses\Utilities;

class EstimateJobPrice 
{
    /**
     * Get country currency from
     * 
     * @param $currency_code 
     * @return 
     */
    protected function getCountryCurrencyFrom($currency_code)
    {
        if ($currency_code == "NG") {
            return "NGN";
        } else if ($currency_code == "CA") {
            return "CA";
        } else if ($currency_code == "US") {
            return "USD";
        } else if ($currency_code == "GB") {
            return "GBP";
        } else if ($currency_code == "AU") {
            return "EUR";
        } else if ($currency_code == "BE") {
            return "EUR";
        } else if ($currency_code == "CY") {
            return "EUR";
        } else if ($currency_code == "EE") {
            return "EUR";
        } else if ($currency_code == "FI") {
            return "EUR";
        } else if ($currency_code == "FR") {
            return "EUR";
        } else if ($currency_code == "DE") {
            return "EUR";
        } else if ($currency_code == "GI") {
            return "EUR";
        } else if ($currency_code == "IE") {
            return "EUR";
        } else if ($currency_code == "IT") {
            return "EUR";
        } else if ($currency_code == "LV") {
            return "EUR";
        } else if ($currency_code == "LT") {
            return "EUR";
        } else if ($currency_code == "LU") {
            return "EUR";
        } else if ($currency_code == "MT") {
            return "EUR";
        } else if ($currency_code == "NL") {
            return "EUR";
        } else if ($currency_code == "PT") {
            return "EUR";
        } else if ($currency_code == "SK") {
            return "EUR";
        } else if ($currency_code == "SI") {
            return "EUR";
        } else if ($currency_code == "ES") {
            return "EUR";
        } else if ($currency_code == "AE") {
            return "AED";
        } else {
            return "USD";
        }
    }

    /**
     * Get exchange rate
     * 
     * @return 
     */
    protected function exchangeRateFor($amount, $from, $to)
    {
        $url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
        $data = file_get_contents($url);
        preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return round($converted, 3);
    }

    /**
     * Make sure the dbPrice is lower than the convertedPrice
     * 
     * @param $dbPrice        
     * @param $convertedPrice 
     * @return                 
     */
    protected function makeSureNewPriceBiggerThanActualPrice($dbPrice, $convertedPrice)
    {
        while ($convertedPrice <= $dbPrice) {
            $convertedPrice = $convertedPrice + 500;
        }

        return $convertedPrice;
    }

    /**
     * get actual price per country_code
     * 
     * @return 
     */
    public function getActualPriceFrom($price, $country_code)
    {
        $currency = $this->getCountryCurrencyFrom($country_code);

        if ($currency == "NGN") 
        {
            return $price;
        }
        if ($currency == "CA")
        {
            $currency_converted_rate = $this->exchangeRateFor(1, "CAD", "NGN");

            $newPrice = round(($price / 216) * $currency_converted_rate);

            return $this->makeSureNewPriceBiggerThanActualPrice($price, $newPrice);
        }
        if ($currency == "GBP")
        {
            $currency_converted_rate = $this->exchangeRateFor(1, "GBP", "NGN");

            $newPrice = round(($price / 357) * $currency_converted_rate);

            return $this->makeSureNewPriceBiggerThanActualPrice($price, $newPrice);
        }
        if ($currency == "USD")
        {
            $currency_converted_rate = $this->exchangeRateFor(1, "USD", "NGN");

            $newPrice = round(($price / 275) * $currency_converted_rate);

            return $this->makeSureNewPriceBiggerThanActualPrice($price, $newPrice);
        }
        if ($currency == "EUR")
        {
            $currency_converted_rate = $this->exchangeRateFor(1, "EUR", "NGN");

            $newPrice = round(($price / 324) * $currency_converted_rate);

            return $this->makeSureNewPriceBiggerThanActualPrice($price, $newPrice);
        }
        if ($currency == "AED")
        {
            $currency_converted_rate = $this->exchangeRateFor(1, "AED", "NGN");

            $newPrice = round(($price / 75) * $currency_converted_rate);

            return $this->makeSureNewPriceBiggerThanActualPrice($price, $newPrice);
        }
    }
}
