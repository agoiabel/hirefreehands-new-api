<?php

namespace App\HelperClasses\Utilities;

class TokenGenerator 
{
	/**
	 * Generate token
	 * 
	 * @return 
	 */
	public function token()
	{
		return uniqid() . time();
	}
}