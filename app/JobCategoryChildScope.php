<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategoryChildScope extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_category_id', 'name'
    ];

    /**
     * A jobcategorychildscope belongs to a category
     * 
     * @return 
     */
    public function category()
    {
    	return $this->belongsTo(JobCategory::class, 'job_category_id');
    }

    /**
     * this has many jobs
     * 
     * @return 
     */
    public function jobs()
    {
        return $this->hasMany(Job::class, 'job_category_child_scope_id');
    }
}
