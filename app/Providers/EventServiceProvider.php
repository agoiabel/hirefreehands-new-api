<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserWasCreated' => [
            'App\Listeners\SendEmailVerificationLink',
            'App\Listeners\CreateNewUserRole',
            'App\Listeners\CreateNewUserProfile',
        ],
        'App\Events\JobWasCreated' => [
            'App\Listeners\CreateJobAddon',
            'App\Listeners\SendNewJobCreatedMailToClient',
            'App\Listeners\SendNewJobCreatedMailToAdmin',
            'App\Listeners\HandleMilestoneCase'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
