<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobFreelancer extends Model
{
	const NOT_STARTED = '1';
    const STARTED = '2';
	const COMPLETED = '3';

	/**
	 * attr that can be mass assigned
	 * @var [type]
	 */
    protected $fillable = [
    	'job_id', 'job_milestone_id', 'freelancer_id', 'skill_group_id', 'freelancer_signed', 'status', 'freelancer_seen'
    ];    

    /**
     * a jobFreelancer belongs to a job
     * 
     * @return 
     */
    public function job()
    {
    	return $this->belongsTo(Job::class, 'job_id');
    }

    /**
     * a jobFreelancer belongs to a freelancer
     * 
     * @return 
     */
    public function freelancer()
    {
    	return $this->belongsTo(User::class, 'freelancer_id');
    }

    /**
     * this belongs to a job_milestone
     * 
     * @return 
     */
    public function job_milestone()
    {
        return $this->belongsTo(JobMilestone::class, 'job_milestone_id');
    }
}
