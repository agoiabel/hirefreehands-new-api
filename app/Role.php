<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	const ADMIN = 1;
	const CLIENT = 2;
	const FREELANCER = 3;

	/**
	 * Attr that can be mass assigned
	 * 
	 * @var array
	 */
    protected $fillable = ['name'];

    /**
     * A role belongs to many users
     * 
     * @return 
     */
    public function userRoles()
    {
    	return $this->hasMany(RoleUser::class, 'role_id');
    }

    /**
     * A user can have one profile
     * 
     * @return 
     */
    public function profile()
    {
    	return $this->hasOne(Profile::class, 'user_id');
    }
    
}
