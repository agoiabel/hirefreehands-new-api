<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    const ACTIVE = true; 
    const INACTIVE = false; 

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name', 'code', 'weight', 'status'
    ];

    /**
     * a coupon can have many jobs
     * 
     * @return 
     */
    public function jobs()
    {
    	return $this->hasMany(Job::class, 'coupon_id');
    }
    
}
