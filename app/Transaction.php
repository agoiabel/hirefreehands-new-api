<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	const VERIFIED = true;
	const PENDING_VERIFICATION = false;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'user_id', 'transaction_id', 'details', 'amount', 'resource_id', 'status', 'type'
   	];

   	/**
   	 * a transaction belong to a user
   	 * 
   	 * @return 
   	 */
   	public function user()
   	{
   		return $this->belongsTo(User::class, 'user_id');
   	}
}
