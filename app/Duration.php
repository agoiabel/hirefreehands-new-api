<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'name'
    ];

    /**
     * a duration has many ScopeDurationPoint
     * 
     * @return 
     */
	public function scope_duration_points()
    {
    	return $this->hasMany(ScopeDurationPoint::class, 'duration_id');	
    }
}