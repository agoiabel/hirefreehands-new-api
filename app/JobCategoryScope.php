<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategoryScope extends Model
{   

    const has_no_milestone = 0;
    const has_milestone = 1;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_category_id', 'title', 'has_milestone', 'example', 'number_of_days', 'base_price'
    ];

    /**
     * A job category scope belongs to job_category
     * 
     * @return 
     */
    public function job_category()
    {
    	return $this->belongsTo(JobCategory::class, 'job_category_id');
    }

    /**
     * this class has many JobCategoryScope
     * 
     * @return 
     */
    public function scope_duration_points()
    {
        return $this->hasMany(ScopeDurationPoint::class, 'job_category_scope_id');
    }

    /**
     * A job_category_scope has many addons
     * 
     * @return 
     */
    public function job_category_scope_addons()
    {
        return $this->hasMany(JobCategoryScopeAddon::class, 'job_category_scope_id');
    }

    /**
     * this has many scope_features
     * 
     * @return 
     */
    public function scope_features()
    {
        return $this->hasMany(JobCategoryScopeFeature::class, 'job_category_scope_id');    
    }

}
