<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobReview extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = [
    	'job_id', 'rating', 'comment', 'freelancer_id'
    ];

    /**
     * a review belongs to a job
     * 
     * @return 
     */
    public function job()
    {
    	return $this->belongsTo(Job::class, 'job_id');
    }

    /**
     * a job belongs to a freelancer
     * 
     * @return 
     */
    public function freelancer()
    {
    	return $this->belongsTo(User::class, 'freelancer_id');
    }
    
}
