<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobLog extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var 
	 */
    protected $fillable = [
    	'job_id', 'milestone_id', 'details', 'type', 'from_uid', 'to_uid'
    ];

    /**
     * a jobLog belongs to a job
     * 
     * @return 
     */
    public function job()
    {
    	return $this->belongsTo(Job::class, 'job_id');
    }

    /**
     * a jobLog belongs to a milestone
     * 
     * @return 
     */
    public function milestone()
    {
    	return $this->belongsTo(JobMilestone::class, 'job_milestone_id');
    }

    /**
     * a jobLog belongs to sender 
     * 
     * @return 
     */
    public function sender()
    {
    	return $this->belongsTo(User::class, 'from_uid');
    }

    /**
     * a jobLog belongs to a reciever
     * 
     * @return 
     */
    public function receiver()
    {
    	return $this->belongsTo(User::class, 'to_uid');
    }
}
