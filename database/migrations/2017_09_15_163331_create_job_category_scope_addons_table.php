<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryScopeAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_scope_addons', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_category_scope_id')->unsigned();
            $table->integer('category_addon_id')->unsigned();

            $table->foreign('job_category_scope_id')->references('id')->on('job_category_scopes')->onDelete('cascade');
            $table->foreign('category_addon_id')->references('id')->on('category_addons')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_category_scope_addons');
    }
}
