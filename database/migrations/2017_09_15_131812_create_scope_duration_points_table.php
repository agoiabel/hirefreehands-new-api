<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScopeDurationPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scope_duration_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_category_scope_id')->unsigned();
            $table->integer('duration_id')->unsigned();
            $table->integer('day');
            $table->integer('price');

            $table->foreign('job_category_scope_id')->references('id')->on('job_category_scopes')->onDelete('cascade');
            $table->foreign('duration_id')->references('id')->on('durations')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scope_duration_points');
    }
}
