<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->integer('job_milestone_id')->unsigned();
            $table->foreign('job_milestone_id')->references('id')->on('job_milestones')->onDelete('cascade');

            $table->string('details');
            $table->string('type');

            $table->integer('from_uid')->unsigned();
            $table->foreign('from_uid')->references('id')->on('users')->onDelete('cascade');

            $table->integer('to_uid')->unsigned();
            $table->foreign('to_uid')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_logs');
    }
}
