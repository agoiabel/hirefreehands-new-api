<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_addons', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_category_id')->unsigned();
            $table->string('title');
            $table->string('operator');
            $table->boolean('toogle_on_milestone')->default(true);
            $table->integer('weight');

            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_addons');
    }
}
