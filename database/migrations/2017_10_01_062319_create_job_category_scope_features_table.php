<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryScopeFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_scope_features', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('feature_id')->unsigned();
            $table->integer('job_category_scope_id')->unsigned();

            $table->decimal('price');
            $table->integer('no_of_days');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_category_scope_features');
    }
}
