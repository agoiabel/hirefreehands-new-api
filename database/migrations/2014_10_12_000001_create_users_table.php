<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('skill_group_id')->unsigned()->nullable();
            $table->string('uid');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('token')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('active')->default(false);
            $table->boolean('email_verified')->default(false);

            $table->foreign('skill_group_id')->references('id')->on('skill_groups')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
