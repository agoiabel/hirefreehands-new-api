<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_milestones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('job_category_id')->unsigned()->nullable();
            $table->integer('skill_group_id')->unsigned();

            $table->string('title');
            $table->text('description');
            $table->integer('weight');

            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->foreign('skill_group_id')->references('id')->on('skill_groups')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_category_milestones');
    }
}
