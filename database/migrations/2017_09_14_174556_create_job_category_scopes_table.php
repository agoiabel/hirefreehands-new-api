<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_category_id')->unsigned();

            $table->string('title');
            $table->integer('has_milestone')->default(true);
            $table->text('example')->nullable();

            // $table->integer('number_of_days')->nullable();
            // $table->integer('base_price')->nullable();

            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_category_scopes');
    }
}
