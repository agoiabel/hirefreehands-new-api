<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_category_id')->unsigned();
            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');

            $table->integer('coupon_id')->unsigned()->nullable();
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');

            $table->integer('scope_duration_point_id')->unsigned();
            $table->foreign('scope_duration_point_id')->references('id')->on('scope_duration_points')->onDelete('cascade');

            $table->integer('job_category_scope_id')->unsigned();
            $table->foreign('job_category_scope_id')->references('id')->on('job_category_scopes')->onDelete('cascade');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('job_category_child_scope_id')->unsigned()->nullable();
            $table->foreign('job_category_child_scope_id')->references('id')->on('job_category_child_scopes')->onDelete('cascade');

            $table->integer('project_manager_id')->unsigned()->nullable();
            $table->foreign('project_manager_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('current_milestone_id')->nullable();

            $table->string('slug')->nullable();

            $table->string('title')->nullable();
            $table->text('description')->nullable();

            // $table->boolean('client_paid')->default(false);
            $table->boolean('hasMilestones')->default(false);
            
            $table->integer('status')->default(0);

            $table->decimal('discount')->nullable();
            $table->decimal('price')->nullable();

            $table->timestamp('job_start_date')->nullable(); //i need to update this when freelancer signed after client has
            $table->timestamp('job_end_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
