<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_milestones', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');

            $table->boolean('status')->default(false);
            $table->boolean('paid_for')->default(false);

            $table->decimal('price');
            $table->boolean('client_signed')->default(false);

            $table->boolean('client_seen')->default(false);

            $table->timestamp('job_start_date')->nullable();
            $table->timestamp('job_end_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_milestones');
    }
}
