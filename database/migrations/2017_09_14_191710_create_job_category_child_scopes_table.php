<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryChildScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_child_scopes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('job_category_id')->unsigned();
            $table->string('name');

            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_category_child_scopes');
    }
}
