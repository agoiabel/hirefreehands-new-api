<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobFreelancersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_freelancers', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
           
            $table->integer('job_milestone_id')->unsigned();
            $table->foreign('job_milestone_id')->references('id')->on('job_milestones')->onDelete('cascade');

            $table->integer('freelancer_id')->unsigned()->nullable();
            $table->foreign('freelancer_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('skill_group_id')->unsigned()->nullable();
            $table->foreign('skill_group_id')->references('id')->on('skill_groups')->onDelete('cascade');
            
            $table->boolean('freelancer_signed')->default(false);
            $table->boolean('freelancer_seen')->default(false);
            
            $table->integer('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_freelancers');
    }
}






























































