<?php

use App\Coupon;
use Illuminate\Database\Seeder;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coupons = [
        	[
        		'name' => 'General coupon',
        		'code' => 'SAVE15',
        		'weight' => '15',
        		'status' => true,
        	],
        	[
        		'name' => 'General cheaper',
        		'code' => 'SAVE10',
        		'weight' => '10',
        		'status' => true,
        	],
		];

		foreach ($coupons as $coupon) {
			Coupon::create($coupon);
		}
    }
}
