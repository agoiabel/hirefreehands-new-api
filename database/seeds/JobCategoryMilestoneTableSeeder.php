<?php

use App\JobCategory;
use App\JobCategoryMilestone;
use Illuminate\Database\Seeder;

class JobCategoryMilestoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $category_with_name_graphics_design = JobCategory::where('id', '1')->firstOrFail();
        $category_with_name_websites = JobCategory::where('id', '2')->firstOrFail();
        $category_with_name_videos_and_animation = JobCategory::where('id', '3')->firstOrFail();
        $category_with_name_content_writing = JobCategory::where('id', '4')->firstOrFail();

        $job_categories_milestones = [
            //Graphics Design
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Ideation / Conceptualization',
                'description'=>'The idea / inspiration points have been handed in are satisfactory.',
                'weight'=>'30',
                'skill_group_id' => '1' //should be fix later
            ],
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Initial Design / Sketch / Draft',
                'description'=>'The idea has been translated into an initial design  draft.',
                'weight'=>'40',
                'skill_group_id' => '2' //should be fix later
            ],
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Final Design Submission',
                'description'=>'All reviews / corrections on the initial design have been satisfactorily implemented and the final work submitted.',
                'weight'=>'30', 
                'skill_group_id' => '3' //should be fix later
            ],
            //Websites
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'UI, Wireframing & Layout Design',
                'description'=>'The proposed layout and/or design of the site is satisfactory',
                'weight'=>'30', 
                'skill_group_id' => '2'
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Development, Features & Client Testing',
                'description'=>'First version of website has been developed and tested',
                'weight'=>'45', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Corrections Implementation, Testing & Handover',
                'description'=>'All corrections have been implement and complete website has been handed over',
                'weight'=>'25', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            // Video and Animation
            [
                'job_category_id'=>$category_with_name_videos_and_animation->id,
                'title'=> 'Scripting',
                'description'=>'The script/storyline of the animation has been completed and is satisfactory.',
                'weight'=>'10', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            [
                'job_category_id'=>$category_with_name_videos_and_animation->id,
                'title'=> 'Storyboarding',
                'description'=>'The storyboard is sketched out and created showing all scenes of the script.',
                'weight'=>'20', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            [
                'job_category_id'=>$category_with_name_videos_and_animation->id,
                'title'=> 'Initial Draft',
                'description'=>'All elements to be used in the animation have been created and developed into a first version of the video.',
                'weight'=>'30', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            [
                'job_category_id'=>$category_with_name_videos_and_animation->id,
                'title'=> 'Final Submission',
                'description'=>'All corrections and reviews on the initial draft have been satisfactorily implemented and the final video has been handed over.',
                'weight'=>'40', 
                'skill_group_id' => $faker->numberBetween(1,5) //should be fix later
            ],
            

        ];
        
        foreach ($job_categories_milestones as $key => $job_category_milestone) {
            JobCategoryMilestone::create($job_category_milestone);
        }

    }
}
