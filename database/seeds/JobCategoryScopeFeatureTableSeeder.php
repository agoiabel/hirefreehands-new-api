<?php

use App\JobCategoryScope;
use Illuminate\Database\Seeder;
use App\JobCategoryScopeFeature;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JobCategoryScopeFeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

        foreach (JobCategoryScope::where('job_category_id', '5')->get() as $key => $job_category_scope) {        
            foreach (range(1, 5) as $key => $value) {

                $feature_id = $faker->numberBetween($min = 1, $max = 3);

                try {
                    JobCategoryScopeFeature::where('feature_id', $feature_id)
                                            ->where('job_category_scope_id', $job_category_scope->id)
                                            ->firstOrFail();
                } catch (ModelNotFoundException $e) {
                	JobCategoryScopeFeature::create([
            			'feature_id' => $feature_id,
            			'job_category_scope_id' => $job_category_scope->id,
                        'price' => $faker->numberBetween($min=1000, $max=5000),
                        'no_of_days' => $faker->numberBetween($min=10, $max=50),
                	]);
                }

            }
        }
    }

}
