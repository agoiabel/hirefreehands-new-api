<?php

use App\User;
use App\Role;
use App\Profile;
use App\RoleUser;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        #client user
        $client = User::create([
        	'uid' => '12345',
        	'firstname' => $faker->firstNameMale,
        	'lastname' => $faker->lastname,
        	'email' => 'hfhclient@mailinator.com',
        	'password' => 'abc',
        	'active' => User::ACTIVE,
        	'email_verified' => User::EMAIL_VERIFIED,
        ]);
        Profile::create([
        	'user_id' => $client->id
        ]);
        RoleUser::create([
        	'user_id' => $client->id,
        	'role_id' => Role::CLIENT
        ]);

        #freelancer user
        $freelancer = User::create([
        	'uid' => '12345',
        	'firstname' => $faker->firstNameMale,
        	'lastname' => $faker->lastname,
        	'email' => 'hfhfreelancer@mailinator.com',
        	'password' => 'abc',
        	'active' => User::ACTIVE,
        	'email_verified' => User::EMAIL_VERIFIED,
            'skill_group_id' => '1' //should be fix later
        ]);
        Profile::create([
        	'user_id' => $freelancer->id
        ]);
        RoleUser::create([
        	'user_id' => $freelancer->id,
        	'role_id' => Role::FREELANCER
        ]);

        #admin user
        $admin = User::create([
        	'uid' => '12345',
        	'firstname' => $faker->firstNameMale,
        	'lastname' => $faker->lastname,
        	'email' => 'hfhadmin@mailinator.com',
        	'password' => 'abc',
        	'active' => User::ACTIVE,
        	'email_verified' => User::EMAIL_VERIFIED,
        ]);
        Profile::create([
        	'user_id' => $admin->id
        ]);
        RoleUser::create([
        	'user_id' => $admin->id,
        	'role_id' => Role::ADMIN
        ]);
    }
}
