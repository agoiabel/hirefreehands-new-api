<?php

use App\SkillGroup;
use Illuminate\Database\Seeder;

class SkillGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();
        
        foreach (range(1, 5) as $key => $value) {
        	SkillGroup::create([
        		'name' => $faker->jobTitle
        	]);
        }
    }
}
