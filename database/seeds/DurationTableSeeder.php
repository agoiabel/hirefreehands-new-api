<?php

use App\Duration;
use Illuminate\Database\Seeder;

class DurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $durations = [
        	['name' => 'regular'],
        	['name' => 'express'],
        	['name' => 'fast'],
        	['name' => 'steady'],
        	['name' => 'patient'],
        ];

        foreach ($durations as $duration) {
        	Duration::create($duration);
        }
    }
}
