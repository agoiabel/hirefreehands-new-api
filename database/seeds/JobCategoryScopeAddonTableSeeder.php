<?php

use App\JobCategory;
use App\CategoryAddon;
use App\JobCategoryScopeAddon;
use Illuminate\Database\Seeder;

class JobCategoryScopeAddonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach (CategoryAddon::get() as $key => $category_addon) {
          foreach (JobCategory::where('id', $category_addon->job_category_id)->firstOrFail()->scopes as $key => $scope) {
            JobCategoryScopeAddon::create([
              'job_category_scope_id' => $scope->id,
              'category_addon_id' => $category_addon->id
            ]);
          }
        }

    }

}
