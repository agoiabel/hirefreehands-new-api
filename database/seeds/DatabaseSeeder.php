<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * $table to seed
     * @var array
     */
    protected $table = [
        'skill_groups',
        'roles',
        'job_categories',
        'job_category_scopes',
        'job_category_child_scopes',
        'durations',
        'scope_duration_points',
        'job_category_milestones',
        'job_category_scope_addons',
        'category_addons',
        'coupons',
        'users',
        'features',
        'job_category_scope_features'
    ];


    /**
     * call all seeder class
     * 
     * @var []
     */
    protected $seeder = [
        'SkillGroupTableSeeder',
        'RolesTableSeeder',
        'JobCategoryTableSeeder',
        'JobCategoryScopeTableSeeder',
        'JobCategoryChildScopeTableSeeder',
        'DurationTableSeeder',
        'ScopeDurationPointTableSeeder',
        'JobCategoryMilestoneTableSeeder',
        'CategoryAddonTableSeeder',
        'JobCategoryScopeAddonTableSeeder',
        'CouponTableSeeder',
        'UserTableSeeder',
        'FeatureTableSeeder',
        'JobCategoryScopeFeatureTableSeeder'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->cleanDatabase();

        foreach($this->seeder as $seedClass)
        {
            $this->call($seedClass);
        }
    }


    /**
     * trucate the database for a new seed
     * 
     * @return 
     */
    protected function cleanDatabase()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach($this->table as $table)
        {
            DB::table($table)->truncate();
        }   

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }


}