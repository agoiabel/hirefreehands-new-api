<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	['name' => 'ADMIN'],
        	['name' => 'CLIENT'],
        	['name' => 'FREELANCER'],
        ];

        foreach ($roles as $key => $role) {
        	Role::create($role);
        }
    }
}
