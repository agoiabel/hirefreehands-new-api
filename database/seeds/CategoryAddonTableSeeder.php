<?php

use App\JobCategory;
use App\CategoryAddon;
use Illuminate\Database\Seeder;

class CategoryAddonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$category_with_name_graphics_design = JobCategory::where('id', '1')->firstOrFail();
        $category_with_name_websites = JobCategory::where('id', '2')->firstOrFail();
        $category_with_name_videos_and_animation = JobCategory::where('id', '3')->firstOrFail();
        $category_with_name_content_writing = JobCategory::where('id', '4')->firstOrFail();

        $category_addons = [
            // Graphics Design
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Make 5 More Corrections',
                'toogle_on_milestone'=>CategoryAddon::toogle_on_milestone,
                'weight'=>'35',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Provide 2 Creative Options to Choose From',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'60',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Offer Copywriting & Content Development',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_graphics_design->id,
                'title'=> 'Add Business Card, Letterhead & Invoice Design [For Logo Jobs]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'75',
                'operator'=>'addition',
            ],
            // Websites
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Website Content Development',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Search Engine Optimization',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'10',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Social Media & Feed Integration [Facebook, Twitter, Instagram, RSS]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'5',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'UI Design [Logo & Brand Design not included]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'50',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Online Payment Integration',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Webmail & Server Deployment [Domain Name & Server Purchase not included]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Inventory Management System [To List and Update Products]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Shopping Cart Integration',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Mobile Responsiveness',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Stock Photos',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Form Builders [e.g Contact Forms to Collect Info]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'CMS [Content Management System e.g. Wordpress, Magento]',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'25',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_websites->id,
                'title'=> 'Modify an Existing Website?',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'10',
                'operator'=>'addition',
            ],
            // Video and Animation
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Provide 2 Creative Options to Choose From',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'45',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Produce 2 or More Separate Cuts',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> '2 or More File Formats',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'20',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Script Writing',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'35',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Include Voiceover Recording',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Stock Photos',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Include Background Music',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'25',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Animated Character Design (1-3 Characters)',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Animated Character Design (3+ Characters)',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'60',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Text Overlay (Subtitles)',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'10',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> '3D Modelling',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'60',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Video Storyboard Design Illustration / Sketch',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'35',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_videos_and_animation->id,
                'title'=> 'Include 1-2 Extra Spokespersons / Actors',
                'toogle_on_milestone'=>CategoryAddon::toogle_off_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            // Content Writing
            [
                'job_category_id'=> $category_with_name_content_writing->id,
                'title'=> '3 Extra Corrections to Work',
                'toogle_on_milestone'=>CategoryAddon::toogle_on_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_content_writing->id,
                'title'=> '1,000 More Words in Content [Articles, Blog Posts & Press Releases]',
                'toogle_on_milestone'=>CategoryAddon::toogle_on_milestone,
                'weight'=>'40',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_content_writing->id,
                'title'=> 'Offer Additional Research Services',
                'toogle_on_milestone'=>CategoryAddon::toogle_on_milestone,
                'weight'=>'60',
                'operator'=>'addition',
            ],
            [
                'job_category_id'=> $category_with_name_content_writing->id,
                'title'=> 'Implementing Corrections / Rewriting Work after Proofreading ',
                'toogle_on_milestone'=>CategoryAddon::toogle_on_milestone,
                'weight'=>'30',
                'operator'=>'addition',
            ],

        ];


        foreach ($category_addons as $key => $category_addon) {
        	CategoryAddon::create($category_addon);
        }
    }
}