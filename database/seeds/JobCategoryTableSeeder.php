<?php

use App\JobCategory;
use Illuminate\Database\Seeder;

class JobCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_categories = [
        	[
        		'name' => 'graphics design',
        		'type' => 'digital',
        		'milestone_weight' => '40',
        		'banner' => 'graphic-design.jpg',
                'percentage_charge' => '20'
        	],
        	[
        		'name' => 'web design and development',
        		'type' => 'digital',
        		'milestone_weight' => '60',
        		'banner' => 'web-digital-banner.jpg',
                'has_child_scope' => true,
                'percentage_charge' => '20'
        	],
        	[
        		'name' => 'video and animation',
        		'type' => 'digital',
        		'milestone_weight' => '75',
        		'banner' => 'animation.jpg',
                'percentage_charge' => '20'
        	],
        	[
        		'name' => 'content writing',
        		'type' => 'digital',
        		'milestone_weight' => '50',
        		'banner' => 'animation.jpg',
                'percentage_charge' => '20'
        	],
            [
                'name' => 'Application',
                'type' => 'hfhe',
                'milestone_weight' => '50',
                'banner' => 'animation.jpg',
                'percentage_charge' => '25'
            ],
        ];

        foreach ($job_categories as $key => $job_category) {
        	JobCategory::create($job_category);
        }
    }
}
    