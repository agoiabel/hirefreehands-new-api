<?php

use App\Duration;
use App\JobCategoryScope;
use App\ScopeDurationPoint;
use Illuminate\Database\Seeder;

class ScopeDurationPointTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $scope_duration_points = [
        
        // Graphic Banner / Flyer Design
            [
                // Regular
                'job_category_scope_id' => '1',
                'duration_id' => '1',
                'day' => '3',
                'price' => '10000'                
            ],
            [
                // Express
                'job_category_scope_id' => '1',
                'duration_id' => '2',
                'day' => '1',
                'price' => '17000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '1',
                'duration_id' => '3',
                'day' => '2',
                'price' => '13000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '1',
                'duration_id' => '4',
                'day' => '4',
                'price' => '8000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '1',
                'duration_id' => '5',
                'day' => '5',
                'price' => '7000'                
            ],
    
        // Single Creative Concept Design
            [
                // Regular
                'job_category_scope_id' => '2',
                'duration_id' => '1',
                'day' => '3',
                'price' => '12000'                
            ],
            [
                // Express
                'job_category_scope_id' => '2',
                'duration_id' => '2',
                'day' => '1',
                'price' => '20400'                
            ],
            [
                // Fast
                'job_category_scope_id' => '2',
                'duration_id' => '3',
                'day' => '2',
                'price' => '15600'                
            ],
            [
                // Steady
                'job_category_scope_id' => '2',
                'duration_id' => '4',
                'day' => '4',
                'price' => '9600'                
            ],
            [
                // Patient
                'job_category_scope_id' => '2',
                'duration_id' => '5',
                'day' => '5',
                'price' => '9000'                
            ],

          // Designing for Websites and Website Elements

            [
                // Regular
                'job_category_scope_id' => '3',
                'duration_id' => '1',
                'day' => '7',
                'price' => '10000'                
            ],
            [
                // Express
                'job_category_scope_id' => '3',
                'duration_id' => '2',
                'day' => '3',
                'price' => '17000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '3',
                'duration_id' => '3',
                'day' => '5',
                'price' => '13000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '3',
                'duration_id' => '4',
                'day' => '9',
                'price' => '8000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '3',
                'duration_id' => '5',
                'day' => '12',
                'price' => '7500'                
            ],

          // Complete Brand Manual Designs

            [
                // Regular
                'job_category_scope_id' => '4',
                'duration_id' => '1',
                'day' => '7',
                'price' => '48000'                
            ],
            [
                // Express
                'job_category_scope_id' => '4',
                'duration_id' => '2',
                'day' => '3',
                'price' => '60000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '4',
                'duration_id' => '3',
                'day' => '5',
                'price' => '55000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '4',
                'duration_id' => '4',
                'day' => '9',
                'price' => '43000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '4',
                'duration_id' => '5',
                'day' => '12',
                'price' => '40000'                
            ],
            
            //  Designing for Publications
            [
                // Regular
                'job_category_scope_id' => '5',
                'duration_id' => '1',
                'day' => '21',
                'price' => '105000'                
            ],
            [
                // Express
                'job_category_scope_id' => '5',
                'duration_id' => '2',
                'day' => '7',
                'price' => '178500'                
            ],
            [
                // Fast
                'job_category_scope_id' => '5',
                'duration_id' => '3',
                'day' => '14',
                'price' => '136500'                
            ],
            [
                // Steady
                'job_category_scope_id' => '5',
                'duration_id' => '4',
                'day' => '28',
                'price' => '84000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '5',
                'duration_id' => '5',
                'day' => '35',
                'price' => '78750'                
            ],

            // Single Page
            [
                // Regular
                'job_category_scope_id' => '6',
                'duration_id' => '1',
                'day' => '7',
                'price' => '32000'                
            ],
            [
                // Express
                'job_category_scope_id' => '6',
                'duration_id' => '2',
                'day' => '3',
                'price' => '40000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '6',
                'duration_id' => '3',
                'day' => '5',
                'price' => '37000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '6',
                'duration_id' => '4',
                'day' => '12',
                'price' => '28000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '6',
                'duration_id' => '5',
                'day' => '16',
                'price' => '30500'                
            ],  

            // 2-5 pages
            [
                // Regular
                'job_category_scope_id' => '7',
                'duration_id' => '1',
                'day' => '10',
                'price' => '48000'                
            ],
            [
                // Express
                'job_category_scope_id' => '7',
                'duration_id' => '2',
                'day' => '5',
                'price' => '60000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '7',
                'duration_id' => '3',
                'day' => '9',
                'price' => '54000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '7',
                'duration_id' => '4',
                'day' => '18',
                'price' => '44000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '7',
                'duration_id' => '5',
                'day' => '24',
                'price' => '42000'                
            ],

            // 6-10 pages
            [
                // Regular
                'job_category_scope_id' => '8',
                'duration_id' => '1',
                'day' => '18',
                'price' => '70000'                
            ],
            [
                // Express
                'job_category_scope_id' => '8',
                'duration_id' => '2',
                'day' => '7',
                'price' => '85000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '8',
                'duration_id' => '3',
                'day' => '12',
                'price' => '77000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '8',
                'duration_id' => '4',
                'day' => '21',
                'price' => '65000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '8',
                'duration_id' => '5',
                'day' => '28',
                'price' => '63000'                
            ], 


            // 10+ Pages
            [
                // Regular
                'job_category_scope_id' => '9',
                'duration_id' => '1',
                'day' => '24',
                'price' => '90000'                
            ],
            [
                // Express
                'job_category_scope_id' => '9',
                'duration_id' => '2',
                'day' => '12',
                'price' => '120000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '9',
                'duration_id' => '3',
                'day' => '18',
                'price' => '105000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '9',
                'duration_id' => '4',
                'day' => '28',
                'price' => '80000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '9',
                'duration_id' => '5',
                'day' => '35',
                'price' => '75000'                
            ],  


            // Animated Logos & Quick Intros [15 secs Max]
            [
                // Regular
                'job_category_scope_id' => '10',
                'duration_id' => '1',
                'day' => '7',
                'price' => '45000'                
            ],
            [
                // Express
                'job_category_scope_id' => '10',
                'duration_id' => '2',
                'day' => '2',
                'price' => '70000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '10',
                'duration_id' => '3',
                'day' => '4',
                'price' => '60000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '10',
                'duration_id' => '4',
                'day' => '9',
                'price' => '40000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '10',
                'duration_id' => '5',
                'day' => '12',
                'price' => '37500'                
            ],

            // Whiteboard & Illustrated Explainer Videos [90 secs Max]   
                     [
                // Regular
                'job_category_scope_id' => '11',
                'duration_id' => '1',
                'day' => '11',
                'price' => '50000'                
            ],
            [
                // Express
                'job_category_scope_id' => '11',
                'duration_id' => '2',
                'day' => '4',
                'price' => '92000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '11',
                'duration_id' => '3',
                'day' => '7',
                'price' => '78000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '11',
                'duration_id' => '4',
                'day' => '15',
                'price' => '45000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '11',
                'duration_id' => '5',
                'day' => '21',
                'price' => '40000'                
            ],

            // Promotional Videos & Brand Commercials [60 secs Max]
            [
                // Regular
                'job_category_scope_id' => '12',
                'duration_id' => '1',
                'day' => '14',
                'price' => '102000'                
            ],
            [
                // Express
                'job_category_scope_id' => '12',
                'duration_id' => '2',
                'day' => '7',
                'price' => '175000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '12',
                'duration_id' => '3',
                'day' => '11',
                'price' => '145000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '12',
                'duration_id' => '4',
                'day' => '18',
                'price' => '85000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '12',
                'duration_id' => '5',
                'day' => '21',
                'price' => '75000'                
            ],

            // Recorded Testimonial Videos [60 secs, 1 Spokesperson Max]

            [
                // Regular
                'job_category_scope_id' => '13',
                'duration_id' => '1',
                'day' => '7',
                'price' => '30000'                
            ],
            [
                // Express
                'job_category_scope_id' => '13',
                'duration_id' => '2',
                'day' => '3',
                'price' => '50000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '13',
                'duration_id' => '3',
                'day' => '5',
                'price' => '42000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '13',
                'duration_id' => '4',
                'day' => '9',
                'price' => '25000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '13',
                'duration_id' => '5',
                'day' => '12',
                'price' => '23000'                
            ],

            // Animated Videos & Cartoon Ilustrations [300 secs Max]
            [
                // Regular
                'job_category_scope_id' => '14',
                'duration_id' => '1',
                'day' => '24',
                'price' => '180000'                
            ],
            [
                // Express
                'job_category_scope_id' => '14',
                'duration_id' => '2',
                'day' => '9',
                'price' => '306000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '14',
                'duration_id' => '3',
                'day' => '15',
                'price' => '234000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '14',
                'duration_id' => '4',
                'day' => '35',
                'price' => '144000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '14',
                'duration_id' => '5',
                'day' => '45',
                'price' => '135000'                
            ],


            // Offer Creative Copywriting Services for Social Media [10 Captions/Posts Max
            [

                // Regular
                'job_category_scope_id' => '15',
                'duration_id' => '1',
                'day' => '3',
                'price' => '6000'                
            ],
            [
                // Express
                'job_category_scope_id' => '15',
                'duration_id' => '2',
                'day' => '1',
                'price' => '9000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '15',
                'duration_id' => '3',
                'day' => '2',
                'price' => '7500'                
            ],
            [
                // Steady
                'job_category_scope_id' => '15',
                'duration_id' => '4',
                'day' => '4',
                'price' => '5000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '15',
                'duration_id' => '5',
                'day' => '5',
                'price' => '4500'                
            ],

            // Create Articles, Blog Posts or Press Releases [Max...
            [
                // Regular
                'job_category_scope_id' => '16',
                'duration_id' => '1',
                'day' => '3',
                'price' => '7000'                
            ],
            [
                // Express
                'job_category_scope_id' => '16',
                'duration_id' => '2',
                'day' => '1',
                'price' => '11900'                
            ],
            [
                // Fast
                'job_category_scope_id' => '16',
                'duration_id' => '3',
                'day' => '2',
                'price' => '9100'                
            ],
            [
                // Steady
                'job_category_scope_id' => '16',
                'duration_id' => '4',
                'day' => '4',
                'price' => '5600'                
            ],
            [
                // Patient
                'job_category_scope_id' => '16',
                'duration_id' => '5',
                'day' => '5',
                'price' => '5250'                
            ],


            // Create Brief Professional Documents [Max 3 Pages]
            [
                // Regular
                'job_category_scope_id' => '17',
                'duration_id' => '1',
                'day' => '3',
                'price' => '6000'                
            ],
            [
                // Express
                'job_category_scope_id' => '17',
                'duration_id' => '2',
                'day' => '1',
                'price' => '9000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '17',
                'duration_id' => '3',
                'day' => '2',
                'price' => '7500'                
            ],
            [
                // Steady
                'job_category_scope_id' => '17',
                'duration_id' => '4',
                'day' => '4',
                'price' => '5000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '17',
                'duration_id' => '5',
                'day' => '5',
                'price' => '4500'                
            ],


            // Offer Proof Reading Services [20,000 Words Max]
            [
                // Regular
                'job_category_scope_id' => '18',
                'duration_id' => '1',
                'day' => '7',
                'price' => '23000'                
            ],
            [
                // Express
                'job_category_scope_id' => '18',
                'duration_id' => '2',
                'day' => '3',
                'price' => '34000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '18',
                'duration_id' => '3',
                'day' => '5',
                'price' => '27500'                
            ],
            [
                // Steady
                'job_category_scope_id' => '18',
                'duration_id' => '4',
                'day' => '9',
                'price' => '20000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '18',
                'duration_id' => '5',
                'day' => '12',
                'price' => '18000'                
            ],

            // Create Busines and Legal Documents
            [
                // Regular
                'job_category_scope_id' => '19',
                'duration_id' => '1',
                'day' => '14',
                'price' => '75000'                
            ],
            [
                // Express
                'job_category_scope_id' => '19',
                'duration_id' => '2',
                'day' => '7',
                'price' => '135000'                
            ],
            [
                // Fast
                'job_category_scope_id' => '19',
                'duration_id' => '3',
                'day' => '11',
                'price' => '10000'                
            ],
            [
                // Steady
                'job_category_scope_id' => '19',
                'duration_id' => '4',
                'day' => '18',
                'price' => '65000'                
            ],
            [
                // Patient
                'job_category_scope_id' => '19',
                'duration_id' => '5',
                'day' => '21',
                'price' => '55000'                
            ],
        ];

        foreach ($scope_duration_points as $key => $scope_duration_point) {
            ScopeDurationPoint::create($scope_duration_point);
        }
    }

}