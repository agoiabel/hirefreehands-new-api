<?php

use App\Feature;
use Illuminate\Database\Seeder;

class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $features = [
        	[	
        		'name' => 'payment system',
        	],
        	[	
        		'name' => 'Ads',
        		// 'no_of_days' => '14'
        	],
        	[	
        		'name' => 'Contact Form',
        		// 'no_of_days' => '15'
        	],
        ];

        foreach ($features as $key => $feature) {
            Feature::create($feature);
        }
    }
}
