<?php

use App\JobCategory;
use App\JobCategoryScope;
use Illuminate\Database\Seeder;

class JobCategoryScopeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_with_name_graphics_design = JobCategory::where('id', '1')->firstOrFail();
        $category_with_name_websites = JobCategory::where('id', '2')->firstOrFail();
        $category_with_name_videos_and_animation = JobCategory::where('id', '3')->firstOrFail();
        $category_with_name_content_writing = JobCategory::where('id', '4')->firstOrFail();
        $category_with_hfhe = JobCategory::where('id', '5')->firstOrFail();

        $job_categories_scopes = [

            //graphics_design
            [
                'job_category_id' => $category_with_name_graphics_design->id,
                'title' => 'Graphic Banner / Flyer Design',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => 'Design a Batch of Flyers [5 Flyers Max], Design a Batch of 5 Web Banners/Social Media Posts [5 Designs Max]',
            ],
            [
                'job_category_id' => $category_with_name_graphics_design->id,
                'title' => 'Single Creative Concept Design',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => 'Design a Single Professional Logo or Album Art, Design a Single Business Card [Excluding Logo]',
            ],
            [
                'job_category_id' => $category_with_name_graphics_design->id,
                'title' => 'Designing for Websites and Website Elements',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => 'Design a Simple Webpage UI [1 Webpage], Photo Cropping & Editing [10 Images Max]',
            ],
            [
                'job_category_id' => $category_with_name_graphics_design->id,
                'title' => 'Complete Brand Manual Designs',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => 'Design a complete Brand Book illustrating your Logo, Letterhead, Business Cards, Product Packaging Illustrations, Font & Color Schemes etc.',
            ],
            [
                'job_category_id' => $category_with_name_graphics_design->id,
                'title' => 'Designing for Publications',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => 'Edit and Format a Book, Brochure, Magazine or Prospectus [Max 60 Pages]',
            ],
            // Websites
            [
                'job_category_id' => $category_with_name_websites->id,
                'title' => 'Single Page',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_websites->id,
                'title' => '2-5 pages',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_websites->id,
                'title' => '6-10 pages',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_websites->id,
                'title' => '10+ Pages',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            // Video and Animations
            [
                'job_category_id' => $category_with_name_videos_and_animation->id,
                'title' => 'Animated Logos & Quick Intros [15 secs Max]',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_videos_and_animation->id,
                'title' => 'Whiteboard & Illustrated Explainer Videos [90 secs Max]',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_videos_and_animation->id,
                'title' => 'Promotional Videos & Brand Commercials [60 secs Max]',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            [
                'job_category_id' => $category_with_name_videos_and_animation->id,
                'title' => 'Recorded Testimonial Videos [60 secs, 1 Spokesperson Max]',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Get a Recorded Testimonial Video for Your Business by a Professional Spokesperson.',
            ],
            [
                'job_category_id' => $category_with_name_videos_and_animation->id,
                'title' => 'Animated Videos & Cartoon Ilustrations [300 secs Max]',
                'has_milestone' => JobCategoryScope::has_milestone,
                'example' => '',
            ],
            // Content Writing
            [
                'job_category_id' => $category_with_name_content_writing->id,
                'title' => 'Offer Creative Copywriting Services for Social Media [10 Captions/Posts 
                	Max',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Develop 10 Catchy Social Media Post to be deployed across Facebook, Twitter and Instagram, Create 10 Standard Social Media Brand Response Templates for Customer Service Relations',
            ],
            [
                'job_category_id' => $category_with_name_content_writing->id,
                'title' => 'Create Articles, Blog Posts or Press Releases [Max 800 Words]',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Write Up a Creative Blog Post ',
            ],
            [
                'job_category_id' => $category_with_name_content_writing->id,
                'title' => 'Create Brief Professional Documents [Max 3 Pages]',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Create a Professional 3 Paged CV, Create an Application/Cover Letter and Letter of Recommendation ',
            ],
            [
                'job_category_id' => $category_with_name_content_writing->id,
                'title' => 'Offer Proof Reading Services [20,000 Words Max]',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Proofread a Final Year Project, Manuscript, Business Plan or Contract',
            ],
            [
                'job_category_id' => $category_with_name_content_writing->id,
                'title' => 'Create Busines and Legal Documents',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Develop a Professional Business Plan up to 15 Pages, Create a Legal Agreement/Contract for Business or any other purposes',
            ],


            //hfhe
            [
                'job_category_id' => $category_with_hfhe->id,
                'title' => 'Web Platform',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'just a web application',
            ],
            [
                'job_category_id' => $category_with_hfhe->id,
                'title' => 'IOS App',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'iOS mobile',
            ],
            [
                'job_category_id' => $category_with_hfhe->id,
                'title' => 'Andriod App',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Andriod App',
            ],
            [
                'job_category_id' => $category_with_hfhe->id,
                'title' => 'Hybrid App',
                'has_milestone' => JobCategoryScope::has_no_milestone,
                'example' => 'Andriod App',
            ],

        ];

        foreach ($job_categories_scopes as $key => $job_category_scope) {
        	JobCategoryScope::create($job_category_scope);
        }


    }
}