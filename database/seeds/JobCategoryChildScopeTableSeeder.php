<?php

use App\JobCategory;
use App\JobCategoryChildScope;
use Illuminate\Database\Seeder;

class JobCategoryChildScopeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$category_id = (new JobCategory())->where('id', '2')->firstOrFail()->id;

    	$child_scopes = [
			[
		        'job_category_id' => $category_id,
	        	'name' => 'Company/Product website'
	        ],
			[
		        'job_category_id' => $category_id,
	        	'name' => 'Content website'
	        ],
			[
		        'job_category_id' => $category_id,
	        	'name' => 'Personal/Brand website'
	        ],
    	];


    	foreach ($child_scopes as $key => $child_scope) {
	        JobCategoryChildScope::create($child_scope);
    	}
    }
}
