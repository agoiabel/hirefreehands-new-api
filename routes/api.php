<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
	
	Route::post('register/store', ['as' => 'register.store', 'uses' => 'RegistrationController@store']);
	Route::get('verify/email/{slug}', ['as' => 'register.verify', 'uses' => 'RegistrationController@verifyEmail']);
	Route::post('login/store', ['as' => 'login.store', 'uses' => 'AuthenticationController@store']);

	/**
	 * Guard against unauthenticated users
	 */
	Route::middleware(['ApiAuth'])->group(function () {

		Route::get('category/index', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
		Route::get('category/show/{category_id}', ['as' => 'category.show', 'uses' => 'CategoryController@show']);

		Route::get('coupon/index', ['as' => 'coupon.index', 'uses' => 'CouponController@index']);


		Route::post('sendtoken/store', ['as' => 'sendtoken.store', 'uses' => 'SendtokenController@store']);
		Route::post('job/store', ['as' => 'job.store', 'uses' => 'JobController@store']);
		Route::get('job/index', ['as' => 'job.index', 'uses' => 'JobController@index']);


		Route::post('sign_agreement', ['as' => 'sign_agreement', 'uses' => 'PaymentController@sign_agreement']);

		Route::post('initiate_payment', ['as' => 'initiate_payment', 'uses' => 'PaymentController@initialize']);
		Route::post('verify_payment', ['as' => 'verify_payment', 'uses' => 'PaymentController@verify']);
		Route::post('job/set_freelancer', ['as' => 'job_set_freelancer', 'uses' => 'FreelancerController@set_single_freelancer']);

		Route::get('job/show/{job_slug}', ['as' => 'job.show', 'uses' => 'JobController@show']);
		Route::post('job/update_seen', ['as' => 'job.seen', 'uses' => 'JobController@seen']);

		#todo:: Route::post('mailer/send', ['mailer.send', 'uses' => 'SendMailController@store']);

	});

	
});